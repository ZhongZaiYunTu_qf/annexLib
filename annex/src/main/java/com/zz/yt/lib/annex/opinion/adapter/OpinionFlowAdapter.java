package com.zz.yt.lib.annex.opinion.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.opinion.bean.OpinionFlowBean;


/**
 * 意见流转区(列表)
 *
 * @author qf
 * @version 2020-04-08
 */
public class OpinionFlowAdapter extends BaseQuickAdapter<OpinionFlowBean, BaseViewHolder> {


    public OpinionFlowAdapter() {
        super(R.layout.haian_item_opinion_flow);
    }


    @Override
    protected void convert(@NonNull BaseViewHolder helper, @NonNull OpinionFlowBean item) {
        helper.setImageResource(R.id.id_image_icon, item.getImage())
                .setText(R.id.id_text_full_name, item.getFullName())
                .setText(R.id.id_text_state, item.getType())
                .setTextColor(R.id.id_text_state, item.getTypeColor())
                .setText(R.id.id_text_time, item.getTime());

        final String text = item.getText();
        if (TextUtils.isEmpty(text)) {
            helper.setGone(R.id.id_text, true);
        } else {
            helper.setText(R.id.id_text, text).setGone(R.id.id_text, false);
        }
        final int size = getData().size() - 1;
        if (helper.getLayoutPosition() == size) {
            helper.setGone(R.id.id_view_progress, true);
        }

    }

}
