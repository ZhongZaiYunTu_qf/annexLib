package com.zz.yt.lib.annex.picture.adapter;


import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.listener.OnClickPictureListener;
import com.zz.yt.lib.annex.popup.view.DeletePopup;
import com.zz.yt.lib.annex.utils.HeadUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * 图片附件
 *
 * @author qf
 * @version 2020-04-08
 */
public class PictureAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    /**
     * 设置最大数量
     */
    private int selectMax = 9;

    /**
     * 设置显示添加
     */
    private boolean isIncrease = true;
    /**
     * 设置显示删除
     */
    private boolean isDelete = true;

    private List<AnnexBean> mData;

    private OnClickPictureListener mItemClickListener;

    public PictureAdapter() {

    }

    public void setSelectMax(int selectMax) {
        this.selectMax = selectMax;
    }

    public void setDelete(boolean delete) {
        this.isDelete = delete;
    }

    public void setIncrease(boolean isIncrease) {
        this.isIncrease = isIncrease;
    }

    public void setOnItemClickListener(OnClickPictureListener listener) {
        this.mItemClickListener = listener;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void addData(AnnexBean e) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        mData.add(e);
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void addData(List<AnnexBean> e) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        mData.addAll(e);
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void addAllData(List<String> e) {
        if (mData == null) {
            mData = new ArrayList<>();
        }
        for (String path : e) {
            mData.add(new AnnexBean(path));
        }
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void remove(int position) {
        if (mData != null && mData.size() > position) {
            mData.remove(position);
            notifyItemRemoved(position);
        }
    }

    public List<AnnexBean> getData() {
        return mData;
    }

    public List<Object> getDataImage() {
        List<Object> urlArray = new ArrayList<>();
        final int size = mData == null ? 0 : mData.size();
        for (int i = 0; i < size; i++) {
            urlArray.add(mData.get(i).getPath());
        }
        return urlArray;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(Latte.getActivity()).inflate(R.layout.haian_item_picture, parent, false);
        return new BaseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BaseViewHolder helper, int position) {
        final ImageView imageDelete = helper.getView(R.id.id_image_delete);
        final int size = mData == null ? 0 : mData.size();
        if (position == getItemCount() - 1 && isIncrease && size < selectMax) {
            helper.setImageResource(R.id.id_image_picture, R.drawable.hf_increase_picture)
                    .setVisible(R.id.id_image_delete, false);
            helper.getView(R.id.id_image_picture).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onPhotoClick();
                    }
                }
            });
        } else {
            imageDelete.setVisibility(isDelete ? View.VISIBLE : View.GONE);
            imageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int index = helper.getLayoutPosition();
                    if (index != RecyclerView.NO_POSITION) {
                        DeletePopup.create()
                                .setOnClickDeleteListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (mItemClickListener != null) {
                                            mItemClickListener.remove(index);
                                        }
                                    }
                                })
                                .popup();
                    }
                }
            });
            final Object path = mData.get(position).getPath();
            // 展示图片
            HeadUtils.image((ImageView) helper.getView(R.id.id_image_picture), path);

            helper.getView(R.id.id_image_picture).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index = helper.getLayoutPosition();
                    if (index != RecyclerView.NO_POSITION) {
                        if (mItemClickListener != null) {
                            mItemClickListener.onItemClick(v, index);
                        }
                    }
                }
            });

        }
    }

    @Override
    public int getItemCount() {
        if (mData != null && mData.size() >= selectMax) {
            return selectMax;
        } else {
            if (isIncrease) {
                return mData == null ? 1 : mData.size() + 1;
            } else {
                return mData == null ? 0 : mData.size();
            }
        }
    }

}
