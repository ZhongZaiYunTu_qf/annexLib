package com.zz.yt.lib.annex.loader;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.XPopup;
import com.whf.android.jar.app.Latte;


import java.util.List;

/**
 * 多张图片预览
 *
 * @author qf
 * @version 2020/8/28
 **/
public class ImageLoaderArrayUtils {

    private final ImageView srcView;
    private final List<Object> urls;
    private int currentPosition = 0;

    /**
     * 多张图片预览
     *
     * @param srcView:点击的图片控件，可以传null
     * @param url:
     */
    @NonNull
    public static ImageLoaderArrayUtils create(ImageView srcView, List<Object> url) {
        return new ImageLoaderArrayUtils(srcView, url);
    }

    private ImageLoaderArrayUtils(ImageView srcView, List<Object> url) {
        this.srcView = srcView;
        this.urls = url;
    }

    /**
     * @param currentPosition:默认预览图片列表的下标
     */
    public ImageLoaderArrayUtils setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition < urls.size() ? currentPosition : urls.size() - 1;
        return this;
    }

    public void show() {
        new XPopup.Builder(Latte.getActivity())
                .asImageViewer(srcView, currentPosition, urls, null,
                        new ImageLoader())
                .show();
    }
}
