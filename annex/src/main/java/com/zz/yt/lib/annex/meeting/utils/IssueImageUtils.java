package com.zz.yt.lib.annex.meeting.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ScreenUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

/**
 * @author qf
 * @version 2020/9/22
 **/
public final class IssueImageUtils {

    private final Context context;

    public static IssueImageUtils init(Context context) {
        return new IssueImageUtils(context);
    }

    private IssueImageUtils(Context context) {
        this.context = context;
    }

    public void setImageHolder(final ImageView imageView, String img) {
        if (imageView != null) {
            imageView.setMinimumWidth(ScreenUtils.getScreenHeight());
            Glide.with(context)
                    //图片地址
                    .load(img)
                    .apply(new RequestOptions().dontTransform())
                    //需要显示的ImageView控件
                    .into(imageView);
            Glide.with(context)
                    .asBitmap()
                    .load(img)
                    .into(new SimpleTarget<Bitmap>() {

                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, Transition<? super Bitmap> transition) {
                            DisplayMetrics dm = new DisplayMetrics();
                            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                            if (wm != null) {
                                wm.getDefaultDisplay().getMetrics(dm);
                                // 屏幕宽度（像素）
                                float widthWin = dm.widthPixels;
                                // 图片宽度（像素）
                                float width = resource.getWidth();
                                float height = resource.getHeight();
                                //缩放比例
                                float ratio = widthWin / width;
                                int heightG = (int) (ratio * height);
                                //设置最优显示的高度
                                imageView.setMinimumHeight(heightG);
                            }
                        }
                    });
        }
    }

    /**
     * 动态加载图片的高度和宽度
     *
     * @param img:
     * @param imageView:
     */
    public void setImage(final ImageView imageView, String img) {
        Glide.with(context)
                .load(img)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e,
                                                Object model,
                                                Target<Drawable> target,
                                                boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource,
                                                   Object model,
                                                   Target<Drawable> target,
                                                   DataSource dataSource,
                                                   boolean isFirstResource) {
                        if (imageView == null) {
                            return false;
                        }
                        if (imageView.getScaleType() != ImageView.ScaleType.FIT_XY) {
                            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        }
                        ViewGroup.LayoutParams params = imageView.getLayoutParams();
                        int width = imageView.getWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight();
                        int height = (int) ((float) width * (float) 1.41);
                        params.height = height + imageView.getPaddingTop() + imageView.getPaddingBottom();
                        imageView.setLayoutParams(params);
                        return true;
                    }
                })
                .into(imageView);
    }

}
