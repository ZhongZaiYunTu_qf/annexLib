package com.zz.yt.lib.annex.listener;


/**
 * 附件回调
 *
 * @author qf
 * @version 1.0.31
 **/
public interface OnClickAnnexCallback{

    /**
     * 删除
     *
     * @param position：
     */
    void remove(int position);
}
