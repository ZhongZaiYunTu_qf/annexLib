package com.zz.yt.lib.annex.listener;

import android.view.View;

/**
 * 图片事件
 *
 * @author qf
 * @version 1.0.10
 **/
public interface OnClickPictureListener {

    /**
     * 点击添加图片
     */
    void onPhotoClick();

    /**
     * 点击预览图片
     *
     * @param view：
     * @param position：
     */
    void onItemClick(View view, int position);

    /**
     * 删除
     *
     * @param position：
     */
    void remove(int position);
}
