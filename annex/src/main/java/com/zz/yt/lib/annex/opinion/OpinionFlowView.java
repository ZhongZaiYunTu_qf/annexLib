package com.zz.yt.lib.annex.opinion;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.opinion.adapter.OpinionAvatarAdapter;
import com.zz.yt.lib.annex.opinion.adapter.OpinionFlowAdapter;
import com.zz.yt.lib.annex.opinion.bean.OpinionFlowBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 流程意见流转区
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 2020/10/29
 **/
public class OpinionFlowView extends LinearLayout {

    private final RecyclerView mRecyclerView;
    private boolean isAvatar;
    private OpinionAvatarAdapter mAvatarAdapter;
    private OpinionFlowAdapter mAnnexAdapter;

    public OpinionFlowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // 加载布局
        LayoutInflater.from(context).inflate(R.layout.haian_view_opinion_flow, this, true);
        mRecyclerView = findViewById(R.id.recycler_opinion_flow);
        final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.OpinionFlowView);

        //是否有头像
        final boolean isAvatar = attributes.getBoolean(R.styleable.OpinionFlowView_is_avatar_opinion, false);

        //设置adapter
        setAvatar(isAvatar);

        //列表显示
        LinearLayoutManager manager = new LinearLayoutManager(context);
        mRecyclerView.setLayoutManager(manager);
        //其他属性

        attributes.recycle();
    }

    public void setAvatar(boolean avatar) {
        isAvatar = avatar;
        if (isAvatar) {
            mAvatarAdapter = new OpinionAvatarAdapter();
            mRecyclerView.setAdapter(mAvatarAdapter);
        } else {
            mAnnexAdapter = new OpinionFlowAdapter();
            mRecyclerView.setAdapter(mAnnexAdapter);
        }
    }

    public void setData(List<OpinionFlowBean> result) {
        if (isAvatar) {
            if (mAvatarAdapter != null) {
                mAvatarAdapter.setList(result);
            }
        } else {
            if (mAnnexAdapter != null) {
                mAnnexAdapter.setList(result);
            }
        }
    }

    public void addData(List<OpinionFlowBean> result) {
        if (isAvatar) {
            if (mAvatarAdapter != null) {
                mAvatarAdapter.addData(result);
            }
        } else {
            if (mAnnexAdapter != null) {
                mAnnexAdapter.addData(result);
            }
        }
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public List<OpinionFlowBean> getData() {
        if (isAvatar) {
            if (mAvatarAdapter != null) {
                return mAvatarAdapter.getData();
            }
        } else {
            if (mAnnexAdapter != null) {
                return mAnnexAdapter.getData();
            }
        }
        return new ArrayList<>();
    }


}