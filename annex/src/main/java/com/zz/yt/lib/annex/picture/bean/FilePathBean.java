package com.zz.yt.lib.annex.picture.bean;

import java.io.Serializable;

/**
 * 上传的附件
 *
 * @author qf
 * @version 1.0
 **/
public class FilePathBean implements Serializable {

    /***
     * A文件id
     */
    private String id;

    /***
     * B文件id
     */
    private String fileId;

    /***
     * 文件存放包
     */
    private String bucketName;

    /**
     * 文件名称
     */
    private String name = "";

    /***
     * 文件名称
     */
    private String fileName;

    /***
     * A 网络地址
     */
    private String path;

    /***
     * B 网络地址
     */
    private String filePath;

    /**
     * 业务效果id
     */
    private String businessId;

    /**
     * 类型：自定义表单时传"uploadFile"
     */
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
