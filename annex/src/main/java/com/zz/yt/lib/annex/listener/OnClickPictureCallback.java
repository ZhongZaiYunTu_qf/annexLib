package com.zz.yt.lib.annex.listener;



import java.util.List;

/**
 * 图片选择回调
 *
 * @author qf
 * @version 2020/8/10
 **/
public interface OnClickPictureCallback {


    /**
     * 图片选择回调
     *
     * @param selectList：
     */
    void onCallback(List<String> selectList);

    /**
     * 去掉选择图片
     *
     * @param position：
     */
    void remove(int position);

}
