package com.zz.yt.lib.annex.document.adapter;

import android.view.View;
import android.widget.ImageView;


import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.document.constants.AnnexKeys;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.listener.OnClickAnnexCallback;
import com.zz.yt.lib.annex.listener.OnClickAnnexListener;


/**
 * 附件(默认列表)
 *
 * @author qf
 * @version 2020-04-08
 */
public class AnnexAdapter extends BaseQuickAdapter<AnnexBean, BaseViewHolder> {

    private boolean isDelete = false;
    private OnClickAnnexCallback mOnAnnexCallback;
    private OnClickAnnexListener mOnAnnexListener;

    public AnnexAdapter() {
        super(R.layout.haian_item_annex);
    }

    public void setDelete(boolean isDelete) {
        this.isDelete = !isDelete;
    }

    public void setOnAnnexCallback(OnClickAnnexCallback callback) {
        this.mOnAnnexCallback = callback;
    }

    public void setOnClickAnnexListener(OnClickAnnexListener listener) {
        this.mOnAnnexListener = listener;
    }

    @Override
    protected void convert(@NonNull final BaseViewHolder helper, @NonNull final AnnexBean item) {
        final int type = item.getType();
        helper.setText(R.id.id_text_title, item.getFileName())
                .setGone(R.id.id_text_size, isDelete);
        ImageView idImageType = helper.getView(R.id.id_image_type);
        idImageType.setImageResource(AnnexKeys.getImageType(type));

        helper.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (mOnAnnexListener != null) {
                    mOnAnnexListener.onClickAnnex(type, item);
                }
            }
        });
        helper.getView(R.id.id_text_size).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnAnnexCallback != null) {
                    mOnAnnexCallback.remove(helper.getLayoutPosition());
                }
            }
        });
    }

}
