package com.zz.yt.lib.annex.meeting.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ObjectUtils;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.document.DocumentView;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.listener.OnClickAnnexLoading;
import com.zz.yt.lib.annex.meeting.entity.IssueEntity;
import com.zz.yt.lib.annex.meeting.utils.IssueImageUtils;

import java.util.ArrayList;
import java.util.List;

/***
 * 议案阅读器
 * 上下滑动型
 * @author gogs
 */
public class IssueListAdapter extends BaseCommonAdapter<IssueEntity> {

    private final OnClickAnnexLoading mClickAnnexLoading;

    public IssueListAdapter(Context context, List<IssueEntity> mList, OnClickAnnexLoading listener) {
        super(context, mList);
        this.mClickAnnexLoading = listener;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        IssuePageHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.haian_item_issue, parent, false);
            holder = new IssuePageHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (IssuePageHolder) convertView.getTag();
        }
        convert(holder, mData.get(position));
        return convertView;
    }

    @SuppressLint("SetTextI18n")
    private void convert(@NonNull IssuePageHolder holder, @NonNull IssueEntity issueEntity) {
        holder.imageView.setVisibility(issueEntity.getType() == 0 ? View.GONE : View.VISIBLE);
        holder.llHead.setVisibility(issueEntity.getType() == 0 ? View.VISIBLE : View.GONE);
        if (issueEntity.getType() == 0) {
            final String issue = issueEntity.getIssue();
            holder.head.setText(issueEntity.getHead());
            holder.headOne.setText(issueEntity.getHeadOne());
            holder.theme.setText(issueEntity.getTheme());
            holder.issue.setText(Html.fromHtml(issue));
            holder.reportDept.setText("呈报部门:  " + issueEntity.getReportDept());
            holder.reportTime.setText("呈报时间:  " + issueEntity.getReportTime());
            holder.reportUser.setText("汇  报  人:  " + issueEntity.getReportUser());
            holder.reportAttend.setText("列  席:  " + issueEntity.getAttendance());
            holder.tel.setText("联系电话:  " + issueEntity.getTel());
            //
            initDocument(holder, issueEntity.getFiles());
        } else {
            final ImageView imageView = holder.imageView;
            IssueImageUtils.init(context)
                    .setImageHolder(imageView, issueEntity.getUrl());
        }
    }

    /***
     * 显示附件
     * @param holder:
     */
    private void initDocument(IssuePageHolder holder, List<String> data) {
        final int size = data == null ? 0 : data.size();
        final ArrayList<AnnexBean> annex = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            annex.add(new AnnexBean(data.get(i)));
        }
        final DocumentView document = holder.documentView;
        if (ObjectUtils.isNotEmpty(document)) {
            document.setEdit(false);
            document.setData(annex);
            document.setOnClickAnnexLoading(mClickAnnexLoading);
        }
    }


    static class IssuePageHolder extends RecyclerView.ViewHolder {

        public TextView head;
        public TextView headOne;
        public TextView theme;
        public TextView issue;
        public TextView reportDept;
        public TextView reportTime;
        public TextView reportUser;
        public TextView reportAttend;
        public TextView tel;
        public AppCompatTextView idTextPage;
        public LinearLayout llHead;
        public DocumentView documentView;
        public ImageView imageView;

        public IssuePageHolder(@NonNull View itemView) {
            super(itemView);
            head = itemView.findViewById(R.id.tv_head);
            headOne = itemView.findViewById(R.id.tv_head_one);
            theme = itemView.findViewById(R.id.tv_theme);
            issue = itemView.findViewById(R.id.tv_issue);
//            enclosure = itemView.findViewById(R.id.tv_enclosure);
            reportDept = itemView.findViewById(R.id.reportDept);
            reportTime = itemView.findViewById(R.id.reportTime);
            reportUser = itemView.findViewById(R.id.reportUser);
            reportAttend = itemView.findViewById(R.id.reportAttend);
            tel = itemView.findViewById(R.id.reportTel);
            idTextPage = itemView.findViewById(R.id.id_text_page);
            llHead = itemView.findViewById(R.id.ll_head);
            documentView = itemView.findViewById(R.id.id_document_view);
            imageView = itemView.findViewById(R.id.img_image);
        }
    }
}
