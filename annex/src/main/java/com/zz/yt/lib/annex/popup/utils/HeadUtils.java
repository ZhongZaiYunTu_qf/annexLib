package com.zz.yt.lib.annex.popup.utils;

import android.widget.ImageView;

import androidx.annotation.DrawableRes;

import com.bumptech.glide.Glide;
import com.bumptech.glide.signature.ObjectKey;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.constants.UserConstant;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.annex.R;

/**
 * 头像
 *
 * @author qf
 * @version 2020/8/11
 **/
@Deprecated
public final class HeadUtils {

    /**
     * 头像类设置头像
     */
    public static void preview(ImageView imageView) {
        preview(imageView, LattePreference.getCustomAppProfile(UserConstant.HEAD_PIC));
    }

    /**
     * 头像类设置头像
     */
    public static void preview(ImageView imageView, String string) {
        preview(imageView, string, R.mipmap.iv_default_head_image);
    }

    /**
     * 头像类设置头像
     */
    public static void preview(ImageView imageView, String string, @DrawableRes int resourceId) {
        Glide.with(Latte.getActivity())
                .load(string)
                .signature(new ObjectKey(System.currentTimeMillis()))
                .circleCrop()
                .error(resourceId)
                .into(imageView);
    }

    /**
     * 设置图片
     */
    public static void image(ImageView imageView, String string) {
        image(imageView, string, R.mipmap.iv_default_head_image);
    }

    /**
     * 设置图片
     */
    public static void image(ImageView imageView, String string, @DrawableRes int resourceId) {
        Glide.with(Latte.getActivity())
                .load(string)
                .signature(new ObjectKey(System.currentTimeMillis()))
                .error(resourceId)
                .into(imageView);
    }

}
