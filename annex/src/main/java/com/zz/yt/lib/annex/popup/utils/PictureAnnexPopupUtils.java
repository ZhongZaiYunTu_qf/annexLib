package com.zz.yt.lib.annex.popup.utils;

import android.app.Activity;
import android.content.Intent;

import androidx.activity.result.ActivityResultLauncher;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.ObjectUtils;
import com.luck.picture.lib.entity.LocalMedia;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.annex.document.util.AnnexUtil;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.picture.utils.AnnexPictureUtil;
import com.zz.yt.lib.annex.popup.PictureAnnexPopup;

import java.util.ArrayList;
import java.util.List;


/**
 * 照片获取方式选择
 *
 * @author qf
 * @version 1.0
 **/
public final class PictureAnnexPopupUtils implements PictureAnnexPopup.OnClickListener,
        AnnexPictureUtil.OnClickListener {

    private Activity mActivity;
    private Fragment mFragment;
    private ActivityResultLauncher<Intent> resultLauncher;
    /*** 可选择文件数量*/
    private int maxCount = 9;

    /*** 是否单张图片选择，默认是false*/
    private boolean isRadio;

    /*** 是否图片裁剪，默认是false*/
    private boolean isCropping;

    /*** 文件选择的*/
    private int reCode = -1;

    private final OnClickPictureCallback mClickPicture;

    @NonNull
    public static PictureAnnexPopupUtils create(OnClickPictureCallback listener) {
        /// 注：有可能是不同控件、不同界面，所以每次需要重新new
        return new PictureAnnexPopupUtils(listener);
    }

    public PictureAnnexPopupUtils setContent(Activity act) {
        this.mActivity = act;
        return this;
    }

    public PictureAnnexPopupUtils setContent(Fragment fra) {
        this.mFragment = fra;
        return this;
    }

    public PictureAnnexPopupUtils setMaxCount(int maxCount) {
        this.maxCount = maxCount;
        return this;
    }

    /**
     * @param radio: 是否单选
     */
    public PictureAnnexPopupUtils setRadio(boolean radio) {
        isRadio = radio;
        return this;
    }

    /**
     * @param cropping: 是否裁剪
     */
    public PictureAnnexPopupUtils setCropping(boolean cropping) {
        isCropping = cropping;
        return this;
    }

    /**
     * @param requestCode：回调的requestCode
     */
    public PictureAnnexPopupUtils setRequestCode(int requestCode) {
        this.reCode = requestCode;
        return this;
    }

    /**
     * @param resultLauncher:api的直接回调方法，处理下文件的那个回调
     */
    public PictureAnnexPopupUtils setResultLauncher(ActivityResultLauncher<Intent> resultLauncher) {
        this.resultLauncher = resultLauncher;
        return this;
    }

    private PictureAnnexPopupUtils(OnClickPictureCallback listener) {
        this.mClickPicture = listener;
        mActivity = Latte.getActivity();
    }

    public final void popup() {
        PictureAnnexPopup.create(Latte.getActivity())
                .setClickListener(this)
                .setPhotograph("拍照")
                .setLocal("相册")
                .setAnnex("文件")
                .popup();
    }

    @Override
    public void requestCamera() {
        setCamera(true);
    }

    @Override
    public void chooseRequest() {
        setCamera(false);
    }


    @Override
    public void requestAnnex() {
        if (resultLauncher != null) {
            AnnexUtil.Bundle()
                    .setMaxCount(maxCount)
                    .setResultLauncher(resultLauncher)
                    .setRequestCode(reCode)
                    .onAddAnnexClickListener();
            return;
        }
        if (ObjectUtils.isNotEmpty(mFragment)) {
            AnnexUtil.Bundle()
                    .setContent(mFragment)
                    .setMaxCount(maxCount)
                    .setRequestCode(reCode)
                    .onAddAnnexClickListener();
            return;
        }
        AnnexUtil.Bundle()
                .setContent(mActivity)
                .setMaxCount(maxCount)
                .setRequestCode(reCode)
                .onAddAnnexClickListener();
    }


    private void setCamera(boolean camera) {
        if (ObjectUtils.isNotEmpty(mFragment)) {
            AnnexPictureUtil.Bundle()
                    .setContent(mFragment)
                    .setCamera(camera)
                    .setMaxCount(maxCount)
                    .setRadio(isRadio)
                    .setCropping(isCropping)
                    .forResult(this)
                    .init();
            return;
        }
        AnnexPictureUtil.Bundle()
                .setContent(mActivity)
                .setCamera(camera)
                .setMaxCount(maxCount)
                .setRadio(isRadio)
                .setCropping(isCropping)
                .forResult(this)
                .init();
    }

    @Override
    public void onClick(List<String> path, ArrayList<LocalMedia> media) {
        if (mClickPicture != null) {
            mClickPicture.onCallback(path);
        }
    }

    @Override
    public void onPreviewDelete(int position) {
        if (mClickPicture != null) {
            mClickPicture.remove(position);
        }
    }
}
