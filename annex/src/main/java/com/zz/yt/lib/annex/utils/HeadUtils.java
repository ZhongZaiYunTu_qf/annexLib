package com.zz.yt.lib.annex.utils;

import android.widget.ImageView;

import androidx.annotation.DrawableRes;
import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.constants.UserConstant;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.annex.dao.ImageErrorDao;

/**
 * 头像
 *
 * @author qf
 * @version 2020/8/11
 **/
public final class HeadUtils extends ImageErrorDao {

    /**
     * 设置图片加载策略
     */
    private static final RequestOptions RECYCLER_OPTIONS_CIRCLE = new RequestOptions()
            .circleCrop()//圆形裁剪
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .dontAnimate();

    /**
     * 设置图片加载策略
     */
    private static final RequestOptions RECYCLER_OPTIONS = new RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .dontAnimate();

    /**
     * 头像类设置头像
     */
    public static void preview(ImageView imageView) {
        preview(imageView, LattePreference.getCustomAppProfile(UserConstant.HEAD_PIC));
    }

    /**
     * 头像类设置头像
     */
    public static void preview(ImageView imageView, Object model) {
        preview(imageView, model, getErrorHead());
    }

    /**
     * 头像类设置头像
     */
    public static void preview(ImageView imageView, Object model, @DrawableRes int resourceId) {
        if (imageView == null) {
            LatteLogger.e("imageView is null ! ");
            return;
        }
        Glide.with(Latte.getActivity())
                .load(model)
                .signature(new ObjectKey(System.currentTimeMillis()))
                .circleCrop()
                .apply(RECYCLER_OPTIONS_CIRCLE)
                .error(resourceId)
                .into(imageView);
    }

    /**
     * 设置图片
     */
    public static void image(ImageView imageView, Object model) {
        image(imageView, model, getErrorImage());
    }

    /**
     * 设置图片
     */
    public static void image(ImageView imageView, Object model, @DrawableRes int resourceId) {
        if (imageView == null) {
            LatteLogger.e("imageView is null ! ");
            return;
        }
        Glide.with(Latte.getActivity())
                .load(model)
                .signature(new ObjectKey(System.currentTimeMillis()))
                .apply(RECYCLER_OPTIONS)
                .error(resourceId)
                .into(imageView);
    }

    /**
     * 设置图片
     */
    public static void image(AppCompatImageView imageView, Object model) {
        image(imageView, model, getErrorImage());
    }


    /**
     * 设置图片
     */
    public static void image(AppCompatImageView imageView, Object model, @DrawableRes int resourceId) {
        if (imageView == null) {
            LatteLogger.e("imageView is null ! ");
            return;
        }
        Glide.with(Latte.getActivity())
                .load(model)
                .signature(new ObjectKey(System.currentTimeMillis()))
                .apply(RECYCLER_OPTIONS)
                .error(resourceId)
                .into(imageView);
    }

}
