package com.zz.yt.lib.annex.opinion.bean;


import com.blankj.utilcode.util.ColorUtils;
import com.zz.yt.lib.annex.R;

import java.io.Serializable;

/**
 * 流程意见区
 *
 * @author qf
 * @version 2020-10-28
 */
public class OpinionFlowBean implements Serializable {

    /**
     * 图片
     */
    private final int IMAGE;

    /**
     * 头像
     */
    private final String AVATAR;

    /**
     * 姓名
     */
    private final String FULL_NAME;

    /**
     * 时间
     */
    private final String TIME;

    /**
     * 类型
     */
    private final String TYPE;

    /**
     * 类型颜色
     */
    private final int TYPE_COLOR;

    /**
     * 内容
     */
    private final String TEXT;

    /**
     * 其他属性
     */
    private Object VALUE;


    public OpinionFlowBean(int image, String fullName, String time, String type, String text) {
        this.IMAGE = image;
        this.AVATAR = null;
        this.FULL_NAME = fullName;
        this.TIME = time;
        this.TYPE = type;
        this.TYPE_COLOR = ColorUtils.getColor(R.color.colorAccent);
        this.TEXT = text;
    }

    public OpinionFlowBean(int image, String avatar, String fullName, String time, String type, String text) {
        this.IMAGE = image;
        this.AVATAR = avatar;
        this.FULL_NAME = fullName;
        this.TIME = time;
        this.TYPE = type;
        this.TYPE_COLOR = ColorUtils.getColor(R.color.colorAccent);
        this.TEXT = text;
    }

    public OpinionFlowBean(int image, String fullName, String time, String type, int typeColor, String text, Object value) {
        this.IMAGE = image;
        this.AVATAR = null;
        this.FULL_NAME = fullName;
        this.TIME = time;
        this.TYPE = type;
        this.TYPE_COLOR = typeColor;
        this.TEXT = text;
        this.VALUE = value;
    }

    public OpinionFlowBean(int image, String avatar, String fullName, String time, String type, int typeColor, String text, Object value) {
        this.IMAGE = image;
        this.AVATAR = avatar;
        this.FULL_NAME = fullName;
        this.TIME = time;
        this.TYPE = type;
        this.TYPE_COLOR = typeColor;
        this.TEXT = text;
        this.VALUE = value;
    }

    public int getImage() {
        return IMAGE;
    }

    public String getAvatar() {
        return AVATAR;
    }

    public String getFullName() {
        return FULL_NAME;
    }

    public String getTime() {
        return TIME;
    }

    public String getType() {
        return TYPE;
    }

    public int getTypeColor() {
        return TYPE_COLOR;
    }

    public String getText() {
        return TEXT;
    }

    public void setValue(Object value) {
        this.VALUE = value;
    }

    public Object getValue() {
        return VALUE;
    }

}
