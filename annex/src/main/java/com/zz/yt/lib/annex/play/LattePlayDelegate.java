package com.zz.yt.lib.annex.play;

import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.blankj.utilcode.util.BarUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.base.delegate.BaseDelegate;
import com.zz.yt.lib.annex.R;


/**
 * 视频界面(mp4等)
 *
 * @author qf
 * @version 2020-04-08
 */
public abstract class LattePlayDelegate extends BaseDelegate implements Player.Listener {

    protected final static String URL = "url";

    protected ExoPlayer player = null;
    protected PlayerView mPlayerView = null;


    protected void initPlayer(PlayerView playerView) {
        if (player == null) {
            player = new ExoPlayer
                    .Builder(context)
                    .build();
            player.addListener(this);
        }
        if (playerView != null) {
            this.mPlayerView = playerView;
            playerView.setPlayer(player);
            playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIT);
            BarUtils.addMarginTopEqualStatusBarHeight(playerView);
        }

        ImageView suspension = findViewById(R.id.exo_suspension);
        if(suspension != null){
            initSuspension(suspension);
        }
        findViewById(R.id.exo_full).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ScreenUtils.isPortrait()) {
                    toFull();
                } else {
                    toHarf();
                }
            }
        });
        findViewById(R.id.exo_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ScreenUtils.isLandscape()) {
                    toHarf();
                } else {
                    getSupportDelegate().pop();
                }
            }
        });

    }

    protected void initSuspension(ImageView suspension) {
        suspension.setVisibility(View.GONE);
    }

    @Override
    public boolean onBackPressedSupport() {
        if (ScreenUtils.isLandscape()) {
            toHarf();
            return true;
        }
        return super.onBackPressedSupport();
    }

    /*** 全屏播放 */
    protected void toFull() {
        BarUtils.setStatusBarVisibility(Latte.getActivity(), false);
        BarUtils.setNavBarVisibility(Latte.getActivity(), false);
        Latte.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        if (mPlayerView != null) {
            mPlayerView.setLayoutParams(layoutParams);
        }
    }

    /*** 回到竖屏 */
    protected void toHarf() {
        BarUtils.setStatusBarVisibility(Latte.getActivity(), true);
        BarUtils.setNavBarVisibility(Latte.getActivity(), true);
        Latte.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                (int) getResources().getDimension(R.dimen.dp_256));
        layoutParams.setMargins(layoutParams.leftMargin,
                layoutParams.topMargin + BarUtils.getStatusBarHeight(),
                layoutParams.rightMargin,
                layoutParams.bottomMargin);
        if (mPlayerView != null) {
            mPlayerView.setLayoutParams(layoutParams);
        }
    }

    /**
     * @param mediaItem:视频文件
     * @param schedule:上次播放进度（秒）
     */
    protected void schedule(MediaItem mediaItem, int schedule) {
        if (player != null) {
            player.setMediaItem(mediaItem);
            player.seekTo(schedule);
            player.prepare();
            player.play();
            //设置视频缩放模式
            player.setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING);
        }
        if (mPlayerView != null) {
            //视频充满屏幕
            mPlayerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FILL);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (player != null) {
            player.setPlayWhenReady(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (player != null) {
            player.release();
            player = null;
        }
    }
}
