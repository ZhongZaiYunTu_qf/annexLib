package com.zz.yt.lib.annex.document.bean;


import androidx.annotation.NonNull;

import com.zz.yt.lib.annex.document.constants.AnnexKeys;

import java.io.Serializable;

/**
 * 附件
 *
 * @author qf
 * @version 2021-06-09
 */
public class AnnexBean implements Serializable {

    /**
     * 文件id
     */
    private final String ID;
    /**
     * 文件名
     * (如:20190605060949.pdf)
     */
    private final String FILE_NAME;

    /**
     * 文件查看路径（网络路径或本地路径）
     * (如:http://58.56.174.102:24894/process/2019-06-05/20190605060949.pdf)
     */
    private final String PATH;

    /**
     * 类型
     * (如:pdf)
     */
    private final @AnnexKeys.TYPE
    int TYPE;

    /**
     * 其他属性
     */
    private Object VALUE;

    /***
     * 本地文件
     * @param relativePath：本地路径
     */
    public AnnexBean(@NonNull String relativePath) {
        final String type = relativePath.substring(relativePath.lastIndexOf(".") + 1);
        this.ID = null;
        this.FILE_NAME = relativePath.substring(relativePath.lastIndexOf("/") + 1);
        this.PATH = relativePath;
        this.TYPE = AnnexKeys.getType(type);
    }

    /***
     * 网络文件
     * @param id：id
     * @param fileName：文件名称
     * @param netWorkPath：网络路径
     */
    public AnnexBean(String id, @NonNull String fileName, @NonNull String netWorkPath) {
        this.ID = id;
        this.FILE_NAME = fileName;
        this.PATH = netWorkPath;
        this.TYPE = AnnexKeys.getFileName(fileName);
    }

    public AnnexBean(String id, @NonNull String fileName, @NonNull String netWorkPath, String size) {
        this.ID = id;
        this.FILE_NAME = fileName;
        this.PATH = netWorkPath;
        this.TYPE = AnnexKeys.getFileName(fileName);
    }

    public AnnexBean(String id, @NonNull String fileName, @NonNull String netWorkPath, Object value) {
        this.ID = id;
        this.FILE_NAME = fileName;
        this.PATH = netWorkPath;
        this.TYPE = AnnexKeys.getFileName(fileName);
        this.VALUE = value;
    }


    public String getId() {
        return ID;
    }

    public String getFileName() {
        return FILE_NAME;
    }

    public int getType() {
        return TYPE;
    }

    public String getPath() {
        return PATH;
    }

    public void setValue(Object value) {
        this.VALUE = value;
    }

    public Object getValue() {
        return VALUE;
    }

}
