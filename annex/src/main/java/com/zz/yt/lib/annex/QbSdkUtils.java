package com.zz.yt.lib.annex;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.blankj.utilcode.util.ObjectUtils;
import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;
import com.tencent.smtt.sdk.TbsVideo;
import com.tencent.smtt.sdk.ValueCallback;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.document.constants.AnnexKeys;
import com.zz.yt.lib.annex.loader.ImageLoaderUtils;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 腾讯（TBS）本地文件打开工具
 * <p>doc、docx、ppt、pptx、xls、xlsx、pdf、txt、epub（支持格式等9种）
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 2020/8/24
 **/
public final class QbSdkUtils {

    /**
     * 位于Latte初始化之后
     */
    public static void init() {
        // 在调用TBS初始化、创建WebView之前进行如下配置
        HashMap<String, Object> map = new HashMap<>(2);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_SPEEDY_CLASSLOADER, true);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_DEXLOADER_SERVICE, true);
        //tbs初始化接口
        QbSdk.initTbsSettings(map);
        //x5内核初始化接口
        QbSdk.initX5Environment(Latte.getApplicationContext(), new QbSdk.PreInitCallback() {

            /**
             * 预初始化结束
             * 由于X5内核体积较大，需要依赖网络动态下发，所以当内核不存在的时候，默认会回调false，此时将会使用系统内核代替
             * @param isX5:是否使用X5内核
             */
            @Override
            public void onViewInitFinished(boolean isX5) {
                //x5內核初始化完成的回调，为true表示x5内核加载成功，
                // 否则表示x5内核加载失败，会自动切换到系统内核。
                LatteLogger.d("app", " onViewInitFinished is " + isX5);
            }

            @Override
            public void onCoreInitFinished() {
                // 内核初始化完成，可能为系统内核，也可能为系统内核
            }
        });

        //允许移动网络下载内核（大小 40-50 MB）。默认移动网络不下载
        QbSdk.setDownloadWithoutWifi(true);
    }

    /**
     * @param context:Activity的context,不能设置为Application的context。
     * @param filePath:文件路径。格式为本地存储路径格式，例如:/sdcard/Download/xxx.doc. 不支持 file:/// 格式。暂不支持在线文件。
     * @param extraParams:扩展功能。为非必填项，若无需特殊配置，默认可传入null。
     * @param callback:打开/关闭时给调用方回调通知
     * @return 1:用 QQ 浏览器打开;2:用 MiniQB 打开; 3:调起阅读器弹框; -1:filePath 为空 打开失败
     */
    public static int openFileReader(Context context,
                                     String filePath,
                                     HashMap<String, String> extraParams,
                                     ValueCallback<String> callback) {
        if (TextUtils.isEmpty(filePath)) {
            return -1;
        }
        final int type = AnnexKeys.getType(filePath.substring(filePath.lastIndexOf(".") + 1));
        //没有安装装QQ浏览器且是图片格式
        if (type == AnnexKeys.JPG || type == AnnexKeys.IMAGE) {
            ImageLoaderUtils.create(null, filePath);
            return 4;
        }
        return QbSdk.openFileReader(context, filePath, extraParams, callback);
    }

    /**
     * @param context:Activity的context,不能设置为Application的context。
     * @param bean:AnnexBean
     * @param extraParams:扩展功能。为非必填项，若无需特殊配置，默认可传入null。
     * @param callback:打开/关闭时给调用方回调通知
     * @return 1:用 QQ 浏览器打开;2:用 MiniQB 打开; 3:调起阅读器弹框; -1:filePath 为空 打开失败
     */
    public static int openFileReader(Context context,
                                     AnnexBean bean,
                                     HashMap<String, String> extraParams,
                                     ValueCallback<String> callback) {
        if (bean == null) {
            return -1;
        }
        //没有安装装QQ浏览器且是图片格式
        if (bean.getType() == AnnexKeys.JPG || bean.getType() == AnnexKeys.IMAGE) {
            ImageLoaderUtils.create(null, bean.getPath());
            return 4;
        }
        return openFileReader(context, bean.getPath(), extraParams, callback);
    }

    public static void closeFileReader(Context context) {
        QbSdk.closeFileReader(context);
    }

    /***
     * 图片、视频查看
     * @param url:图片、视频地址（网址）
     */
    public static boolean inFlue(String url) {
        if (ObjectUtils.isEmpty(url)) {
            return false;
        }
        if (isImage(url)) {
            ImageLoaderUtils.create(null, url);
            return true;
        }
        if (isTxVideo(url)) {
            Bundle bun = new Bundle();
            bun.putInt("screenMode", 102);
            //播放视频
            TbsVideo.openVideo(Latte.getApplicationContext(), url, bun);
            return true;
        }
        return false;
    }

    /***
     * @param filePath:判断是否是图片
     */
    public static boolean isImage(String filePath) {
        if (ObjectUtils.isEmpty(filePath)) {
            return false;
        }
        final String type = filePath.substring(filePath.lastIndexOf(".") + 1);
        final String[] strings = {
                "bmp", "jpg", "png", "tif", "gif",
                "pcx", "tga", "exif", "fpx", "svg",
                "psd", "cdr", "pcd", "dxf", "ufo",
                "eps", "ai", "raw", "WMF", "webp",
                "avif", "apng"};
        final List<String> test = Arrays.asList(strings);
        return test.contains(type);
    }

    /***
     * @param filePath:判断是否可以能用腾讯视频播放
     */
    public static boolean isTxVideo(String filePath) {
        if (ObjectUtils.isEmpty(filePath)) {
            return false;
        }
        final String type = filePath.substring(filePath.lastIndexOf(".") + 1);
        final String[] strings = {
                "mp4", "flv", "avi", "3gp", "webm",
                "ts", "ogv", "m3u8", "asf", "wmv",
                "rm", "rmvb", "mov", "mkv"};
        final List<String> test = Arrays.asList(strings);
        //判断当前是否可用腾讯视频播放
//      return TbsVideo.canUseTbsPlayer(Latte.getApplicationContext()) && test.contains(type);
        return test.contains(type);
    }

    /***
     * <p> doc、docx、xls、xlsx、pdf、txt（支持格式等6种）</>
     * @param filePath:判断pdf.js可以阅览文件
     */
    public static boolean isPdfJs(String filePath) {
        if (ObjectUtils.isEmpty(filePath)) {
            return false;
        }
        final String type = filePath.substring(filePath.lastIndexOf(".") + 1);
        final String[] strings = {"pdf", "doc", "docx", "xls", "xlsx", "txt"};
        final List<String> test = Arrays.asList(strings);
        return test.contains(type);
    }
}
