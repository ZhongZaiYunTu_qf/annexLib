package com.zz.yt.lib.annex.opinion.adapter;

import android.text.TextUtils;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.opinion.bean.OpinionFlowBean;
import com.zz.yt.lib.annex.utils.HeadUtils;

/**
 * 意见流转区(有头像)
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 2020/11/2
 **/
public class OpinionAvatarAdapter extends BaseQuickAdapter<OpinionFlowBean, BaseViewHolder> {


    public OpinionAvatarAdapter() {
        super(R.layout.haian_item_opinion_avatar);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, @NonNull OpinionFlowBean item) {
        final boolean start = helper.getLayoutPosition() != 0;
        helper.setVisible(R.id.id_view_top, start)
                .setImageResource(R.id.id_image_icon, item.getImage())
                .setText(R.id.id_text_full_name, item.getFullName())
                .setText(R.id.id_text_state, item.getType())
                .setTextColor(R.id.id_text_state, item.getTypeColor())
                .setText(R.id.id_text_time, item.getTime());

        //头像
        final ImageView imageView = helper.getView(R.id.id_image_avatar);
        HeadUtils.preview(imageView, item.getAvatar());
        //
        final String text = item.getText();
        if (TextUtils.isEmpty(text)) {
            helper.setGone(R.id.id_text, true);
        } else {
            helper.setText(R.id.id_text, text).setGone(R.id.id_text, false);
        }
        final int size = getData().size() - 1;
        if (helper.getLayoutPosition() == size) {
            helper.setGone(R.id.id_view_progress, true);
        }


    }

}
