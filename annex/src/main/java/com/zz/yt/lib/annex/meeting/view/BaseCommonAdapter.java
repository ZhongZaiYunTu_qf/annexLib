package com.zz.yt.lib.annex.meeting.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * BaseAdapter
 *
 * @author : qf.
 * @author wang.hai.fang
 * @since 2.5.0
 */
abstract class BaseCommonAdapter<T> extends BaseAdapter {

    List<T> mData;
    Context context;
    LayoutInflater mInflater;

    /**
     * @param cont:Context
     * @param mList:List   Date
     */
    BaseCommonAdapter(Context cont, List<T> mList) {
        super();
        this.context = cont;
        this.mData = mList;
        this.mInflater = LayoutInflater.from(context);
    }

    /**
     * Total number
     */
    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public T getItem(int position) {
        return mData.get(position);
    }

    protected void getViewValue() {

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public List<T> getData() {
        getViewValue();
        return mData;
    }

    public boolean hasData() {
        return mData != null && mData.size() > 0;
    }

    public void addItem(T item) {
        getViewValue();
        mData.add(item);
        notifyDataSetChanged();
    }

    public void addData(List<T> mList) {
        mData.addAll(mList);
        notifyDataSetChanged();
    }

    public void updateList(List<T> mList) {
        mData = mList;
        notifyDataSetChanged();
    }

    public T get(int position) {
        if (mData == null) {
            return null;
        }
        return mData.get(position);
    }

    public void deleteItem(int position) {
        getViewValue();
        mData.remove(position);
        notifyDataSetChanged();
    }

    public final void clear() {
        mData.clear();
        notifyDataSetChanged();
    }
}
