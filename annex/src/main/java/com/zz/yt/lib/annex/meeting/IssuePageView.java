package com.zz.yt.lib.annex.meeting;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ObjectUtils;
import com.zz.yt.lib.annex.listener.OnClickAnnexLoading;
import com.zz.yt.lib.annex.meeting.adapter.IssuePageAdapter;
import com.zz.yt.lib.annex.meeting.entity.IssueEntity;
import com.zz.yt.lib.annex.meeting.utils.OnIssueClickListener;
import com.zz.yt.lib.annex.meeting.view.HeadIssueView;
import com.zz.yt.lib.annex.meeting.window.CatalogWindow;

import java.util.ArrayList;
import java.util.List;

/**
 * 阅读会议
 *
 * @author qf
 * @version 2020/9/22
 **/
public class IssuePageView extends RecyclerView implements OnIssueClickListener {

    private final Context context;

    /*** 目录 */
    private final List<IssueEntity> issueList = new ArrayList<>();

    /*** 标题 */
    private String mTitle;

    public IssuePageView(Context context) {
        super(context);
        this.context = context;
        setLayoutManager(new LinearLayoutManager(context));
    }

    public IssuePageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        setLayoutManager(new LinearLayoutManager(context));
    }

    /**
     * 弹框的目录
     */
    public void showWindow() {
        CatalogWindow.onConfirm(context, mTitle, issueList, this);
    }

    /**
     * @param title：会议标题
     * @param issueData：议案
     * @param listener：下载附加的进度条
     */
    public void setData(String title, List<IssueEntity> issueData, OnClickAnnexLoading listener) {
        issueList.clear();
        this.mTitle = title;
        /* 详情 */
        final List<IssueEntity> issueInfoList = new ArrayList<>();
        int size = issueData == null ? 0 : issueData.size();
        for (int i = 0; i < size; i++) {
            IssueEntity entity = issueData.get(i);
            entity.setNumber(getNumber(i));
            //设置
            issueList.add(entity);
            issueInfoList.add(entity);
            //附件
            List<String> files = entity.getFiles();
            if (ObjectUtils.isNotEmpty(files)) {
                for (String url : files) {
                    url = url.replace("\\", "/");
                    issueInfoList.add(new IssueEntity(url));
                }
            }
        }

        IssuePageAdapter adapter = new IssuePageAdapter(issueInfoList, listener);
        setAdapter(adapter);
        setHeadView(adapter);
    }

    /**
     * 设置目录
     */
    private void setHeadView(@NonNull IssuePageAdapter adapter) {
        /// 头
        HeadIssueView changeListView = new HeadIssueView(context);
        adapter.addHeaderView(changeListView);
        changeListView.setData(mTitle, issueList);
        changeListView.setOnClickListener(this);
    }

    private int getNumber(int position) {
        //计算所在行，添加了头，使用从1开始。
        int number = 1;
        if (issueList != null) {
            if (position > 0) {
                for (int i = 0; i < position; i++) {
                    List<String> list = issueList.get(i).getFiles();
                    int isf = list == null ? 1 : list.size() + 1;
                    number = number + isf;
                }
            }
        }
        return number;
    }

    @Override
    public void scroll(int number) {
        if (issueList.size() > number) {
            //跳转到所在行
            moveToPosition(issueList.get(number).getNumber());
        }
    }

    /**
     * 移动到当前位置，
     *
     * @param n 要跳转的位置
     */
    public void moveToPosition(int n) {
        LinearLayoutManager manager = (LinearLayoutManager) getLayoutManager();
        if (manager != null) {
            manager.scrollToPositionWithOffset(n, 0);
            manager.setStackFromEnd(true);
        }
    }

    /**
     * RecyclerView 移动到当前位置，
     *
     * @param manager 设置RecyclerView对应的manager
     * @param num     要跳转的位置
     */
    public void MoveToPosition(@NonNull LinearLayoutManager manager, int num) {
        int firstItem = manager.findFirstVisibleItemPosition();
        int lastItem = manager.findLastVisibleItemPosition();
        if (num <= firstItem) {
            scrollToPosition(num);
        } else if (num <= lastItem) {
            int top = getChildAt(num - firstItem).getTop();
            scrollBy(0, top);
        } else {
            scrollToPosition(num);
        }
    }
}
