package com.zz.yt.lib.annex.document;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.net.RestClient;
import com.whf.android.jar.net.callback.IError;
import com.whf.android.jar.net.callback.IFailure;
import com.whf.android.jar.net.callback.ISuccess;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.annex.QbSdkUtils;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.document.adapter.AnnexGridAdapter;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.listener.OnClickAnnexListener;
import com.zz.yt.lib.annex.listener.OnClickAnnexLoading;

import java.util.ArrayList;
import java.util.List;


/**
 * 文件(doc,pdf等)
 *
 * @author qf
 * @version 2020-04-08
 */
public class DocumentGridView extends RelativeLayout implements OnClickAnnexListener,
        ISuccess, IFailure, IError {

    private final Context context;
    private final boolean isAdded;

    private final RecyclerView mRecyclerView;
    private final AnnexGridAdapter mAnnexGridAdapter;

    private boolean preview = true;
    private String previewHint = "";
    private OnClickAnnexLoading mClickAnnexLoading = null;

    public DocumentGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        isAdded = false;
        // 加载布局
        LayoutInflater.from(context).inflate(R.layout.haian_view_document_grid_picture, this, true);
        mRecyclerView = findViewById(R.id.recyclerview);
        final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.DocumentGridView);

        //
        mAnnexGridAdapter = new AnnexGridAdapter();
        mAnnexGridAdapter.setOnClickAnnexListener(this);
        mRecyclerView.setAdapter(mAnnexGridAdapter);

        //列表显示
        GridLayoutManager manager = new GridLayoutManager(context, 2);
        mRecyclerView.setLayoutManager(manager);
        attributes.recycle();
    }


    /***
     * 不可以查看文件
     * @param preview：提示不可以查看文件的理由。
     */
    public void setPreview(String preview) {
        this.preview = false;
        this.previewHint = preview;
    }

    public void setOnClickAnnexListener(OnClickAnnexLoading listener) {
        this.mClickAnnexLoading = listener;
    }

    public void setData(List<AnnexBean> result) {
        if (mAnnexGridAdapter != null) {
            mAnnexGridAdapter.setList(result);
        }
    }

    public void addData(AnnexBean result) {
        if (mAnnexGridAdapter != null) {
            mAnnexGridAdapter.addData(result);
        }
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public List<AnnexBean> getData() {
        if (mAnnexGridAdapter != null) {
            return mAnnexGridAdapter.getData();
        }
        return new ArrayList<>();
    }

    @Override
    public void onClickAnnex(int type, AnnexBean item) {
        if (!preview) {
            ToastUtils.showShort(previewHint);
            return;
        }
        if (isAdded) {
            QbSdkUtils.openFileReader(context, item, null, null);
        } else {
            if (mClickAnnexLoading != null) {
                mClickAnnexLoading.showLoading();
            }
            RestClient.builder()
                    .url(item.getPath())
                    .name(item.getFileName())
                    .failure(this)
                    .success(this)
                    .error(this)
                    .build()
                    .download();
        }
    }

    @Override
    public void onSuccess(String response) {
        LatteLogger.i("" + response);
        if (mClickAnnexLoading != null) {
            mClickAnnexLoading.hideLoading();
        }
        QbSdkUtils.openFileReader(context, response, null, null);
    }

    @Override
    public void onError(int code, String msg) {
        LatteLogger.e(msg);
        if (mClickAnnexLoading != null) {
            mClickAnnexLoading.hideLoading();
        }
    }

    @Override
    public void onFailure() {
        if (mClickAnnexLoading != null) {
            mClickAnnexLoading.hideLoading();
        }
    }

}
