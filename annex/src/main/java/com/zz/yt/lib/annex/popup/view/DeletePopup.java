package com.zz.yt.lib.annex.popup.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupCenter;
import com.zz.yt.lib.annex.R;


/**
 * 删除提示
 *
 * @author qf
 * @version 2020/4/24
 **/
@SuppressLint("ViewConstructor")
public class DeletePopup extends LattePopupCenter implements View.OnClickListener {

    private View.OnClickListener onClickListener;

    @NonNull
    public static DeletePopup create() {
        return create(Latte.getActivity());
    }

    @NonNull
    public static DeletePopup create(Context context) {
        return new DeletePopup(context);
    }

    private DeletePopup(Context context) {
        super(context);
    }

    @Override
    protected int setLayout() {
        return R.layout.haian_popup_delete;
    }

    public DeletePopup setOnClickDeleteListener(View.OnClickListener l) {
        this.onClickListener = l;
        return this;
    }

    protected void initViews() {
        setOnClickListener(R.id.confirm, this);
        setOnClickListener(R.id.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        dismiss();
        if (onClickListener != null) {
            onClickListener.onClick(v);
        }
    }

}

