package com.zz.yt.lib.annex.document.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.document.constants.AnnexKeys;
import com.zz.yt.lib.annex.listener.OnClickAnnexListener;


/**
 * 附件(九宫格)
 *
 * @author qf
 * @version 2020-04-08
 */
public class AnnexGridAdapter extends BaseQuickAdapter<AnnexBean, BaseViewHolder> {


    private OnClickAnnexListener mOnAnnexListener;

    public AnnexGridAdapter() {
        super(R.layout.haian_item_annex_grid);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, @NonNull final AnnexBean item) {
        final int type = item.getType();
        helper.setText(R.id.id_text_title, item.getFileName());

        final ImageView idImageType = helper.getView(R.id.id_image_type);
        idImageType.setImageResource(AnnexKeys.getImageType(type));

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnAnnexListener != null) {
                    mOnAnnexListener.onClickAnnex(type, item);
                }
            }
        });

    }

    public void setOnClickAnnexListener(OnClickAnnexListener onClickAnnexListener) {
        this.mOnAnnexListener = onClickAnnexListener;
    }


}
