package com.zz.yt.lib.annex.tbs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.ScreenUtils;
import com.blankj.utilcode.util.UtilsTransActivity;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.annex.R;

import java.util.List;

/**
 * 自定义webView
 *
 * @author qf
 * @version 1.0
 * @date 2020/11/5
 **/
public class X5WebActivity extends Activity {

    private final static String URL = "url";

    public static void start(Context context, String playUrl) {
        final Intent intent = new Intent();
        intent.setClass(context, X5WebActivity.class);
        intent.putExtra(URL, playUrl);
        ActivityUtils.startActivity(intent);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.haian_activity_web_x5);
        findViewById(R.id.img_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        inPermission();
    }

    /**
     * 获得权限
     */
    private void inPermission() {
        PermissionUtils
                .permission(PermissionConstants.CAMERA, PermissionConstants.MICROPHONE)
                .rationale(new PermissionUtils.OnRationaleListener() {
                    @Override
                    public void rationale(@NonNull UtilsTransActivity activity, @NonNull ShouldRequest shouldRequest) {
                        shouldRequest.again(true);
                    }
                })
                .theme(new PermissionUtils.ThemeCallback() {
                    @Override
                    public void onActivityCreate(@NonNull Activity activity) {
                        ScreenUtils.setFullScreen(activity);
                    }
                })
                .callback(new PermissionUtils.FullCallback() {
                    @Override
                    public void onGranted(@NonNull List<String> permissionsGranted) {
                        LatteLogger.d("权限获取成功", permissionsGranted);
                        initView();
                    }

                    @Override
                    public void onDenied(@NonNull List<String> permissionsDeniedForever, @NonNull List<String> permissionsDenied) {
                        LatteLogger.d("权限永久拒绝", permissionsDeniedForever);
                        LatteLogger.d("权限拒绝", permissionsDenied);
                        finish();
                    }
                })
                .request();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initView() {
        X5WebView x5Web = findViewById(R.id.x5Web_view);
        String url = "http://www.baidu.com";
        try {
            url = getIntent().getStringExtra(URL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LatteLogger.i(url);
        x5Web.loadUrl(url);
        //系统默认会通过手机浏览器打开网页，为了能够直接通过WebView显示网页，则必须设置
        x5Web.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //使用WebView加载显示url
                view.loadUrl(url);
                //返回true
                return true;
            }
        });
        //是否使用内置缩放机制
        x5Web.getSettings().setDisplayZoomControls(false);
        // 是否支持变焦
        x5Web.getSettings().setSupportZoom(true);
        // 设置WebView是否应该使用其内置变焦机制,显示放大缩小
        x5Web.getSettings().setBuiltInZoomControls(true);
        //是否开启控件viewport。默认false，自适应；true时标签中指定宽度值生效
        x5Web.getSettings().setUseWideViewPort(true);
        x5Web.getSettings().setLoadWithOverviewMode(true);
        // 初始化时缩放
        x5Web.setInitialScale(100);
        //如果访问的页面中要与Javascript交互，则webview必须设置支持Javascript
        x5Web.getSettings().setJavaScriptEnabled(true);
        //支持插件
        x5Web.getSettings().setPluginsEnabled(true);
        //关闭webview中缓存
        x5Web.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        //设置可以访问文件
        x5Web.getSettings().setAllowFileAccess(true);
        //支持通过JS打开新窗口
        x5Web.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        //支持自动加载图片
        x5Web.getSettings().setLoadsImagesAutomatically(true);
        //设置编码格式
        x5Web.getSettings().setDefaultTextEncodingName("utf-8");
    }
}