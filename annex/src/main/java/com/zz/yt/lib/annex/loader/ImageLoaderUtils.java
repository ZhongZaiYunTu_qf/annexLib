package com.zz.yt.lib.annex.loader;

import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.lxj.xpopup.XPopup;
import com.whf.android.jar.app.Latte;

/**
 * 单张图片预览
 *
 * @author qf
 * @version 2020/8/28
 **/
public final class ImageLoaderUtils {

    /**
     * 单张图片预览
     *
     * @param srcView:点击的图片控件，可以传null
     * @param url:
     */
    @NonNull
    public static ImageLoaderUtils create(ImageView srcView, Object url) {
        return new ImageLoaderUtils(srcView, url);
    }

    private ImageLoaderUtils(ImageView srcView, Object url) {
        new XPopup.Builder(Latte.getActivity())
                .asImageViewer(srcView, url, new ImageLoader())
                .show();
    }

}
