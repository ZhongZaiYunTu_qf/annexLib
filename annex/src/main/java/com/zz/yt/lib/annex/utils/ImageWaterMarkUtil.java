package com.zz.yt.lib.annex.utils;


import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.TimeUtils;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.dao.LocateDao;
import com.whf.android.jar.util.DensityUtil;
import com.whf.android.jar.util.bitmap.BitmapUtils;
import com.whf.android.jar.util.log.LatteLogger;
import com.whf.android.jar.util.number.DoubleUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * 给图像添加水印
 *
 * @author pzblog
 * @since 2021-10-29
 */
public class ImageWaterMarkUtil {


    @Nullable
    public static Bitmap watermarkBitmap(String path) {
        List<String> labels = new ArrayList<>();
        labels.add(time());
        labels.add(data());
        labels.add(LocateDao.getAddress());
        return watermarkBitmap(path, labels);
    }

    @NonNull
    public static String time() {
        return TimeUtils.getNowString(TimeUtils.getSafeDateFormat("HH:mm"));
    }

    @NonNull
    public static String data() {
        return TimeUtils.getNowString(TimeUtils.getSafeDateFormat("yyyy.MM.dd EEE"));
    }

    @Nullable
    public static Bitmap watermarkBitmap(String path, List<String> labels) {
        final Bitmap bitmap = BitmapUtils.decodeImage(path);
        if (bitmap == null) {
            LatteLogger.e("bitmap is null");
            return null;
        }
        if (labels == null) {
            return bitmap;
        }
        // 绘制水印
        List<String> labelArray = new ArrayList<>();
        for (String label : labels) {
            if (label.length() > 21) {
                labelArray.add(label.substring(0, 21));
                labelArray.add(label.substring(21));
            } else {
                labelArray.add(label);
            }
        }

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        float spVal = (int) DoubleUtils.div(width, 45);

        //创建一个bitmap
        Bitmap newBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);// 创建一个新的和SRC长度宽度一样的位图

        final Paint paint = new Paint();
        paint.setColor(Color.GRAY); // 水印颜色
        paint.setAntiAlias(true);// 抗锯齿
        paint.setTextSize(DensityUtil.sp2px(Latte.getApplicationContext(), spVal));// 水印字体大小

        final Canvas canvas = new Canvas(newBitmap);
        //在画布 0，0坐标上开始绘制原始图片
        canvas.drawBitmap(bitmap, 0, 0, null);

        // 计算水印的列数
        int size = labelArray.size();
        // 计算水印的位置（这里是图片左下角）
        float x = Math.max(spVal, 10);
        float y = height - (size * x * 3) - x;

        // 绘制水印
        for (int i = 0; i < size; i++) {
            String label = labelArray.get(i);
            canvas.drawText(label, x, y + (i * x * 3), paint);
        }

        canvas.rotate(0);// 水印角度
        // 保存
        canvas.save();
        // 存储
        canvas.restore();

        return newBitmap;
    }

    /**
     * 设置水印图片在左上角
     * <p>
     * 上下文
     *
     * @param src:
     * @param watermark:
     * @param paddingLeft:
     * @param paddingTop:
     */
    public static Bitmap createWaterMaskLeftTop(Bitmap src, Bitmap watermark, int paddingLeft, int paddingTop) {
        return createWaterMaskBitmap(src, watermark,
                dp2px(paddingLeft), dp2px(paddingTop));
    }

    private static Bitmap createWaterMaskBitmap(Bitmap src, Bitmap watermark, int paddingLeft, int paddingTop) {
        if (src == null) {
            return null;
        }
        int width = src.getWidth();
        int height = src.getHeight();
        //创建一个bitmap
        Bitmap newb = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);// 创建一个新的和SRC长度宽度一样的位图
        //将该图片作为画布
        Canvas canvas = new Canvas(newb);
        //在画布 0，0坐标上开始绘制原始图片
        canvas.drawBitmap(src, 0, 0, null);
        //在画布上绘制水印图片
        canvas.drawBitmap(watermark, paddingLeft, paddingTop, null);
        // 保存
        canvas.save();
        // 存储
        canvas.restore();
        return newb;
    }

    /**
     * 设置水印图片在右下角
     * <p>
     * 上下文
     *
     * @param src:
     * @param watermark:
     * @param paddingRight:
     * @param paddingBottom:
     */
    public static Bitmap createWaterMaskRightBottom(Bitmap src, Bitmap watermark, int paddingRight, int paddingBottom) {
        return createWaterMaskBitmap(src, watermark,
                src.getWidth() - watermark.getWidth() - dp2px(paddingRight),
                src.getHeight() - watermark.getHeight() - dp2px(paddingBottom));
    }

    /**
     * 设置水印图片到右上角
     *
     * @param src:
     * @param watermark:
     * @param paddingRight:
     * @param paddingTop:
     */
    public static Bitmap createWaterMaskRightTop(Bitmap src, Bitmap watermark, int paddingRight, int paddingTop) {
        return createWaterMaskBitmap(src, watermark,
                src.getWidth() - watermark.getWidth() - dp2px(paddingRight),
                dp2px(paddingTop));
    }

    /**
     * 设置水印图片到左下角
     *
     * @param src:
     * @param watermark:
     * @param paddingLeft:
     * @param paddingBottom:
     */
    public static Bitmap createWaterMaskLeftBottom(Bitmap src, Bitmap watermark, int paddingLeft, int paddingBottom) {
        return createWaterMaskBitmap(src, watermark, dp2px(paddingLeft),
                src.getHeight() - watermark.getHeight() - dp2px(paddingBottom));
    }

    /**
     * 设置水印图片到中间
     *
     * @param src:
     * @param watermark:
     */
    public static Bitmap createWaterMaskCenter(Bitmap src, Bitmap watermark) {
        return createWaterMaskBitmap(src, watermark,
                (src.getWidth() - watermark.getWidth()) / 2,
                (src.getHeight() - watermark.getHeight()) / 2);
    }

    /**
     * 给图片添加文字到左上角
     *
     * @param bitmap:
     * @param text:
     */
    public static Bitmap drawTextToLeftTop(Bitmap bitmap, String text, int size, int color, int paddingLeft, int paddingTop) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(dp2px(size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return drawTextToBitmap(bitmap, text, paint, bounds,
                dp2px(paddingLeft),
                dp2px(paddingTop) + bounds.height());
    }

    /**
     * 绘制文字到右下角
     *
     * @param bitmap:
     * @param text:
     * @param size:
     * @param color:
     */
    public static Bitmap drawTextToRightBottom(Bitmap bitmap, String text, int size, int color, int paddingRight, int paddingBottom) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(dp2px(size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return drawTextToBitmap(bitmap, text, paint, bounds,
                bitmap.getWidth() - bounds.width() - dp2px(paddingRight),
                bitmap.getHeight() - dp2px(paddingBottom));
    }

    /**
     * 绘制文字到右上方
     *
     * @param bitmap:
     * @param text:
     * @param size:
     * @param color:
     * @param paddingRight:
     * @param paddingTop:
     */
    public static Bitmap drawTextToRightTop(Bitmap bitmap, String text, int size, int color, int paddingRight, int paddingTop) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(dp2px(size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return drawTextToBitmap(bitmap, text, paint, bounds,
                bitmap.getWidth() - bounds.width() - dp2px(paddingRight),
                dp2px(paddingTop) + bounds.height());
    }

    /**
     * 绘制文字到左下方
     *
     * @param bitmap:
     * @param text:
     * @param size:
     * @param color:
     * @param paddingLeft:
     * @param paddingBottom:
     */
    public static Bitmap drawTextToLeftBottom(Bitmap bitmap, String text, int size, int color, int paddingLeft, int paddingBottom) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(dp2px(size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return drawTextToBitmap(bitmap, text, paint, bounds,
                dp2px(paddingLeft),
                bitmap.getHeight() - dp2px(paddingBottom));
    }

    /**
     * 绘制文字到中间
     *
     * @param bitmap:
     * @param text:
     * @param size:
     * @param color:
     */
    public static Bitmap drawTextToCenter(Bitmap bitmap, String text, int size, int color) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(color);
        paint.setTextSize(dp2px(size));
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        return drawTextToBitmap(bitmap, text, paint, bounds,
                (bitmap.getWidth() - bounds.width()) / 2,
                (bitmap.getHeight() + bounds.height()) / 2);
    }

    //图片上绘制文字
    private static Bitmap drawTextToBitmap(Bitmap bitmap, String text, Paint paint, Rect bounds, int paddingLeft, int paddingTop) {
        android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();

        paint.setDither(true); // 获取跟清晰的图像采样
        paint.setFilterBitmap(true);// 过滤一些
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);
        Canvas canvas = new Canvas(bitmap);

        canvas.drawText(text, paddingLeft, paddingTop, paint);
        return bitmap;
    }

    /**
     * 缩放图片
     *
     * @param src:
     * @param w:
     * @param h:
     */
    public static Bitmap scaleWithWH(Bitmap src, double w, double h) {
        if (w == 0 || h == 0 || src == null) {
            return src;
        } else {
            // 记录src的宽高
            int width = src.getWidth();
            int height = src.getHeight();
            // 创建一个matrix容器
            Matrix matrix = new Matrix();
            // 计算缩放比例
            float scaleWidth = (float) (w / width);
            float scaleHeight = (float) (h / height);
            // 开始缩放
            matrix.postScale(scaleWidth, scaleHeight);
            // 创建缩放后的图片
            return Bitmap.createBitmap(src, 0, 0, width, height, matrix, true);
        }
    }

    private static int dp2px(float dpVal) {
        return DensityUtil.dp2px(Latte.getApplicationContext(), dpVal);
    }

}