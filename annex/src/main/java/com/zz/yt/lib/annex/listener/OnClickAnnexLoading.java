package com.zz.yt.lib.annex.listener;


/**
 * 查看附件
 *
 * @author qf
 * @version 2020/7/31
 **/
public interface OnClickAnnexLoading {

    /**
     * 开始下载
     */
    void showLoading();

    /**
     * 下载完成
     */
    void hideLoading();
}
