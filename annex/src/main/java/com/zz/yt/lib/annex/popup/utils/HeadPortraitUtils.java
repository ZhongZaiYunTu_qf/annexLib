package com.zz.yt.lib.annex.popup.utils;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.luck.picture.lib.entity.LocalMedia;
import com.whf.android.jar.app.Latte;
import com.whf.android.jar.constants.UserConstant;
import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.annex.constant.PictureConstant;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.picture.utils.AnnexPictureUtil;
import com.zz.yt.lib.annex.popup.PicturePopup;
import com.zz.yt.lib.annex.utils.HeadUtils;
import com.zz.yt.lib.annex.utils.LocalMediaUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 头像等单个选择工具
 *
 * @author qf
 * @version 2020/8/11
 **/
public final class HeadPortraitUtils implements View.OnClickListener {

    private final ImageView IMAGE_VIEW;
    private final OnClickPictureCallback CALLBACK;
    private int addition = PictureConstant.ALL;
    private boolean circle = true;

    /*** 是否图片裁剪，默认是false*/
    private boolean cropping = false;
    /*** 上下文*/
    private Activity act;
    /*** 上下文*/
    private Fragment mFragment;

    @NonNull
    public static HeadPortraitUtils create(ImageView imageView, OnClickPictureCallback callback) {
        return new HeadPortraitUtils(imageView, callback);
    }

    private HeadPortraitUtils(ImageView imageView, OnClickPictureCallback callback) {
        this.IMAGE_VIEW = imageView;
        this.CALLBACK = callback;
        if (imageView != null) {
            imageView.setOnClickListener(this);
        }
    }

    public HeadPortraitUtils setContent(Activity act) {
        this.act = act;
        return this;
    }

    public HeadPortraitUtils setContent(Fragment mFragment) {
        this.mFragment = mFragment;
        return this;
    }

    /**
     * @param cropping: 是否裁剪
     */
    public HeadPortraitUtils setCropping(boolean cropping) {
        this.cropping = cropping;
        return this;
    }

    /**
     * 图片选择方式
     *
     * @param addition：
     */
    public HeadPortraitUtils setAddition(@PictureConstant.Photograph int addition) {
        this.addition = addition;
        return this;
    }

    /**
     * 不是头像
     */
    public HeadPortraitUtils setNoCircle() {
        this.circle = false;
        return this;
    }

    /**
     * 头像类设置头像
     */
    public void preview() {
        if (circle && IMAGE_VIEW != null) {
            HeadUtils.preview(IMAGE_VIEW);
        }
    }

    @Override
    public void onClick(View v) {
        switch (addition) {
            case PictureConstant.CAMERA:
                openCamera(true);
                break;
            case PictureConstant.GALLERY:
                openCamera(false);
                break;
            case PictureConstant.ALL:
            default:
                PicturePopup.create(Latte.getActivity())
                        .setClickListener(new PicturePopup.OnClickListener() {
                            @Override
                            public void requestCamera() {
                                openCamera(true);
                            }

                            @Override
                            public void chooseRequest() {
                                openCamera(false);
                            }
                        })
                        .popup();
                break;
        }
    }

    /**
     * 照相机
     */
    private void openCamera(boolean camera) {
        AnnexPictureUtil.Bundle()
                .setContent(act)
                .setContent(mFragment)
                .setCamera(camera)
                .setCropping(cropping)
                .setRadio(true)
                .forResult(new AnnexPictureUtil.OnClickListener() {
                    @Override
                    public void onClick(List<String> path, ArrayList<LocalMedia> media) {
                        uoPath(media);
                    }

                    @Override
                    public void onPreviewDelete(int position) {

                    }
                }).init();
    }

    private void uoPath(List<LocalMedia> result) {
        final List<String> objectList = LocalMediaUtils.upPath(result);
        if (result == null || result.size() == 0) {
            return;
        }
        if (IMAGE_VIEW != null && objectList.size() > 0) {
            if (circle) {
                LattePreference.addCustomAppProfile(UserConstant.HEAD_PIC, objectList.get(0) + "");
                HeadUtils.preview(IMAGE_VIEW);
            } else {
                HeadUtils.preview(IMAGE_VIEW, objectList.get(0));
            }
        }
        if (CALLBACK != null) {
            CALLBACK.onCallback(objectList);
        }
    }
}
