package com.zz.yt.lib.annex.popup.utils;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.ObjectUtils;
import com.luck.picture.lib.entity.LocalMedia;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.annex.document.util.AnnexUtil;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.picture.utils.AnnexPictureUtil;
import com.zz.yt.lib.annex.popup.PicturePopup;

import java.util.ArrayList;
import java.util.List;


/**
 * 照片获取方式选择
 * <p> 相册 or 文件</>
 *
 * @author qf
 * @version 2020/6/4
 **/
public final class PicturePopupUtils implements PicturePopup.OnClickListener,
        AnnexPictureUtil.OnClickListener {

    private Activity mActivity;
    private Fragment mFragment;
    /*** 可选择文件数量*/
    private int maxCount = 9;

    /*** 是否单张图片选择，默认是false*/
    private boolean isRadio;

    /*** 是否图片裁剪，默认是false*/
    private boolean isCropping;

    private final OnClickPictureCallback mClickPicture;

    @NonNull
    public static PicturePopupUtils create(OnClickPictureCallback listener) {
        /// 注：有可能是不同控件、不同界面，所以每次需要重新new
        return new PicturePopupUtils(listener);
    }

    public PicturePopupUtils setContent(Activity act) {
        this.mActivity = act;
        return this;
    }

    public PicturePopupUtils setContent(Fragment fra) {
        this.mFragment = fra;
        return this;
    }

    public PicturePopupUtils setMaxCount(int maxCount) {
        this.maxCount = maxCount;
        return this;
    }

    /**
     * @param radio: 是否单选
     */
    public PicturePopupUtils setRadio(boolean radio) {
        isRadio = radio;
        return this;
    }

    /**
     * @param cropping: 是否裁剪
     */
    public PicturePopupUtils setCropping(boolean cropping) {
        isCropping = cropping;
        return this;
    }

    private PicturePopupUtils(OnClickPictureCallback listener) {
        this.mClickPicture = listener;
    }

    public final void popup() {
        PicturePopup.create(Latte.getActivity())
                .setClickListener(this)
                .setPhotograph("相册")
                .setLocal("文件")
                .popup();
    }

    @Override
    public void requestCamera() {
        if (ObjectUtils.isNotEmpty(mFragment)) {
            AnnexPictureUtil.Bundle()
                    .setContent(mActivity)
                    .setMaxSelectNum(maxCount)
                    .setCropping(isCropping)
                    .setRadio(isRadio)
                    .setCamera(false)
                    .forResult(this)
                    .init();
            return;
        }
        if (ObjectUtils.isNotEmpty(mActivity)) {
            AnnexPictureUtil.Bundle()
                    .setContent(mActivity)
                    .setMaxSelectNum(maxCount)
                    .setCropping(isCropping)
                    .setRadio(isRadio)
                    .setCamera(false)
                    .forResult(this)
                    .init();
            return;
        }
        AnnexPictureUtil.Bundle()
                .setMaxSelectNum(maxCount)
                .setCropping(isCropping)
                .setRadio(isRadio)
                .setCamera(false)
                .forResult(this)
                .init();
    }

    @Override
    public void chooseRequest() {
        if (ObjectUtils.isNotEmpty(mFragment)) {
            AnnexUtil.Bundle()
                    .setContent(mFragment)
                    .setMaxCount(maxCount)
                    .onAddAnnexClickListener();
            return;
        }
        if (ObjectUtils.isNotEmpty(mActivity)) {
            AnnexUtil.Bundle()
                    .setContent(mActivity)
                    .setMaxCount(maxCount)
                    .onAddAnnexClickListener();
            return;
        }
        AnnexUtil.Bundle()
                .setMaxCount(maxCount)
                .onAddAnnexClickListener();
    }

    @Override
    public void onClick(List<String> path, ArrayList<LocalMedia> media) {
        if (mClickPicture != null) {
            mClickPicture.onCallback(path);
        }
    }

    @Override
    public void onPreviewDelete(int position) {
        if (mClickPicture != null) {
            mClickPicture.remove(position);
        }
    }

}
