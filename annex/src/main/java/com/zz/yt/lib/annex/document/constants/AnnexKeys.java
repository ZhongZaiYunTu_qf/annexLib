package com.zz.yt.lib.annex.document.constants;

import androidx.annotation.DrawableRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

import com.zz.yt.lib.annex.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 附件类型
 *
 * @author qf
 * @version 2020-01-01
 */
public final class AnnexKeys {

    /**
     * doc
     */
    public final static int DOC = 0;

    /**
     * doc
     */
    public final static int FBX = 1;

    /**
     * JPG
     */
    public final static int JPG = 2;

    /**
     * mp3
     */
    public final static int MP3 = 3;

    /**
     * mp4
     */
    public final static int MP4 = 4;

    /**
     * pdf
     */
    public final static int PDF = 5;

    /**
     * ppt
     */
    public final static int PPT = 6;

    /**
     * rar
     */
    public final static int RAR = 7;

    /**
     * text
     */
    public final static int TEXT = 8;

    /**
     * xls
     */
    public final static int XLS = 9;

    /**
     * 图片
     */
    public final static int IMAGE = 10;

    /**
     * 其他
     */
    public final static int OTHER = 11;

    @IntDef({
            DOC, FBX, JPG, MP3, MP4,
            PDF, PPT, RAR, TEXT, XLS,
            IMAGE, OTHER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TYPE {

    }


    public static int getFileName(@NonNull String fileName) {
        final String type = fileName.substring(fileName.lastIndexOf(".") + 1);
        return getType(type);
    }

    public static int getType(@NonNull String type) {
        switch (type) {
            case "doc":
            case "docx":
                return DOC;
            case "fbx":
                return FBX;
            case "jpg":
            case "png":
            case "jpeg":
            case "JPEG":
                return JPG;
            case "mp3":
                return MP3;
            case "mp4":
                return MP4;
            case "pdf":
            case "PDF":
                return PDF;
            case "ppt":
                return PPT;
            case "rar":
            case "zip":
                return RAR;
            case "text":
                return TEXT;
            case "xls":
            case "xlsx":
                return XLS;
            case "gif":
            case "GIF":
            case "bmp":
            case "tiff":
                return IMAGE;
            default:
                return OTHER;

        }
    }

    public static @DrawableRes
    int getImageType(@TYPE int type) {
        switch (type) {
            case DOC:
                return R.drawable.hf_file_doc;
            case FBX:
                return R.drawable.hf_file_fbx;
            case JPG:
            case IMAGE:
                return R.drawable.hf_file_jpg;
            case MP3:
                return R.drawable.hf_file_mp3;
            case MP4:
                return R.drawable.hf_file_mp4;
            case PDF:
                return R.drawable.hf_file_pdf;
            case PPT:
                return R.drawable.hf_file_ppt;
            case RAR:
                return R.drawable.hf_file_rar;
            case TEXT:
                return R.drawable.hf_file_text;
            case XLS:
                return R.drawable.hf_file_xls;
            case OTHER:
            default:
                return R.drawable.hf_file_unknow;

        }
    }

}
