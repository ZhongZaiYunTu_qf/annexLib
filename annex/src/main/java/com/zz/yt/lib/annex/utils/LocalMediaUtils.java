package com.zz.yt.lib.annex.utils;

import android.os.Build;

import androidx.annotation.NonNull;

import com.luck.picture.lib.entity.LocalMedia;

import java.util.ArrayList;
import java.util.List;


/**
 * 获取照片de路径
 *
 * @author qf
 * @version 2020/6/4
 */
public final class LocalMediaUtils {

    @NonNull
    public static List<String> upPath(List<LocalMedia> result) {
        final List<String> objectList = new ArrayList<>();
        if (result == null || result.size() == 0) {
            return objectList;
        }
        for (LocalMedia media : result) {
            String path;
            if (media.isCut() && !media.isCompressed()) {
                // 裁剪过
                path = media.getCutPath();
            } else if (media.isCompressed() || (media.isCut() && media.isCompressed())) {
                // 压缩过,或者裁剪同时压缩过,以最终压缩过图片为准
                path = media.getCompressPath();
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                // 真实路径
//                path = TextUtils.isEmpty(media.getRealPath()) ? media.getAndroidQToPath() : media.getRealPath();
                path = media.getRealPath();
            } else {
                // 原图
                path = media.getPath();
            }
            objectList.add(path);
        }
        return objectList;
    }
}
