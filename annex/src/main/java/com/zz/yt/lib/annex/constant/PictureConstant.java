package com.zz.yt.lib.annex.constant;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 图片选择方式
 *
 * @author qf
 * @version 2020/11/2
 **/
public final class PictureConstant {

    /**
     * 拍照
     */
    public final static int CAMERA = 1;

    /**
     * 相册
     */
    public final static int GALLERY = 2;

    /**
     * 拍照和相册
     */
    public final static int ALL = 3;

    @IntDef({CAMERA, GALLERY, ALL})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Photograph {

    }

}
