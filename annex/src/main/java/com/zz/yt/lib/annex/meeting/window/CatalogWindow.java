package com.zz.yt.lib.annex.meeting.window;

import android.annotation.SuppressLint;
import android.content.Context;

import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.DrawerPopupView;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.meeting.entity.IssueEntity;
import com.zz.yt.lib.annex.meeting.utils.OnIssueClickListener;
import com.zz.yt.lib.annex.meeting.view.HeadIssueView;

import java.util.List;

/***
 * 弹框的目录
 * @author gogs
 */
@SuppressLint({"ViewConstructor", "NonConstantResourceId"})
public class CatalogWindow extends DrawerPopupView implements OnIssueClickListener {

    private final String mTitle;
    private final List<IssueEntity> issueList;
    private OnIssueClickListener mIssueClickListener;


    public static void onConfirm(Context context, String title,
                                 List<IssueEntity> issueList,
                                 OnIssueClickListener listener) {
        final CatalogWindow popup = new CatalogWindow(context, title, issueList);
        popup.setLinearLayout(listener);
        new XPopup.Builder(Latte.getActivity())
                .isDestroyOnDismiss(true)
                .asCustom(popup)
                .show();
    }

    public CatalogWindow(Context context, String title, List<IssueEntity> issueList) {
        super(context);
        this.mTitle = title;
        this.issueList = issueList;
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.haian_window_issue;
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        HeadIssueView haiList = findViewById(R.id.id_list_view);
        haiList.setData(mTitle, issueList);
        haiList.setOnClickListener(this);

    }

    @Override
    protected void onShow() {
        super.onShow();
    }

    @Override
    protected void onDismiss() {
        super.onDismiss();
    }

    public void setLinearLayout(OnIssueClickListener listener) {
        this.mIssueClickListener = listener;
    }

    @Override
    public void scroll(int number) {
        if (mIssueClickListener != null) {
            mIssueClickListener.scroll(number);
            this.dismiss();
        }
    }

}
