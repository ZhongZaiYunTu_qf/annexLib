package com.zz.yt.lib.annex.meeting.entity;


import java.io.Serializable;
import java.util.List;

/**
 * @author qf
 * @version 2020/9/22
 **/
public final class IssueEntity implements Serializable {

    private String head;
    private String headOne;
    private String reportUser;
    private String issue;
    private String theme;
    private String tel;
    private String reportDept;
    private String attendance;
    private String reportTime;
    private List<String> files;
    private int type = 0;
    private int number = 1;
    private String url;

    public IssueEntity() {
    }

    public IssueEntity(String url) {
        this.type = 2;
        this.url = url;
    }

    public String getHead() {
        return head;
    }

    public String getHeadOne() {
        return headOne;
    }

    public String getReportUser() {
        return reportUser;
    }

    public String getIssue() {
        return issue;
    }

    public String getTheme() {
        return theme;
    }

    public String getTel() {
        return tel;
    }

    public String getReportDept() {
        return reportDept;
    }

    public String getAttendance() {
        return attendance;
    }

    public String getReportTime() {
        return reportTime;
    }

    public List<String> getFiles() {
        return files;
    }

    public int getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public void setHeadOne(String headOne) {
        this.headOne = headOne;
    }

    public void setReportUser(String reportUser) {
        this.reportUser = reportUser;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public void setReportDept(String reportDept) {
        this.reportDept = reportDept;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public void setFiles(List<String> files) {
        this.files = files;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
