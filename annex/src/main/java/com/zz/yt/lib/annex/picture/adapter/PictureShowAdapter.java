package com.zz.yt.lib.annex.picture.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.loader.ImageLoaderArrayUtils;
import com.zz.yt.lib.annex.utils.HeadUtils;


/**
 * 图片附件
 *
 * @author qf
 * @version 2020-04-08
 */
public class PictureShowAdapter extends BaseQuickAdapter<Object, BaseViewHolder> {


    public PictureShowAdapter() {
        super(R.layout.haian_item_picture);
    }

    @Override
    protected void convert(@NonNull final BaseViewHolder helper, Object data) {
        final ImageView imageDelete = helper.getView(R.id.id_image_delete);
        imageDelete.setVisibility(View.GONE);

        HeadUtils.image((ImageView) helper.getView(R.id.id_image_picture), data + "");

        helper.getView(R.id.id_image_picture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = helper.getLayoutPosition();
                if (index != RecyclerView.NO_POSITION) {
                    ImageLoaderArrayUtils.create(null, getData())
                            .setCurrentPosition(index)
                            .show();
                }

            }
        });
    }
}
