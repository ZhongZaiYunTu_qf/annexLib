package com.zz.yt.lib.annex.popup;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.popup.LattePopupBottom;
import com.zz.yt.lib.annex.R;


/**
 * 照片获取方式选择
 *
 * @author qf
 * @version 2020/6/4
 **/
public class PicturePopup extends LattePopupBottom implements View.OnClickListener {

    private String mPhotograph = null;
    private String mLocal = null;
    private OnClickListener mClickListener = null;

    @NonNull
    public static PicturePopup create() {
        return create(Latte.getActivity());
    }

    @NonNull
    public static PicturePopup create(Context context) {
        return new PicturePopup(context);
    }

    private PicturePopup(Context context) {
        super(context);
    }

    @Override
    protected int setLayout() {
        return R.layout.haian_popup_picture;
    }

    @Override
    protected void initViews() {
        setOnClickListener(R.id.button_photograph, this);
        setOnClickListener(R.id.button_local, this);
        setOnClickListener(R.id.button_cancel, this);
        if (mPhotograph != null) {
            setText(R.id.button_photograph, mPhotograph);
        }
        if (mLocal != null) {
            setText(R.id.button_local, mLocal);
        }
    }

    @Override
    public void onClick(View v) {
        dismiss();
        if (mClickListener != null) {
            int id = v.getId();
            if (id == R.id.button_photograph) {
                mClickListener.requestCamera();
            } else if (id == R.id.button_local) {
                mClickListener.chooseRequest();
            }
        }
    }

    /***
     * 事件
     * @param listener：
     */
    public PicturePopup setClickListener(OnClickListener listener) {
        this.mClickListener = listener;
        return this;
    }

    /***
     * 拍照①
     * @param photograph：
     */
    public PicturePopup setPhotograph(String photograph) {
        this.mPhotograph = photograph;
        return this;
    }

    /***
     * 相册②
     * @param local：
     */
    public PicturePopup setLocal(String local) {
        this.mLocal = local;
        return this;
    }

    public interface OnClickListener {

        /**
         * 拍照上传
         */
        void requestCamera();

        /**
         * 本地上传
         */
        void chooseRequest();
    }
}
