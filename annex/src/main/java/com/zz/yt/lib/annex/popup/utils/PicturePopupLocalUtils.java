package com.zz.yt.lib.annex.popup.utils;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.blankj.utilcode.util.ObjectUtils;
import com.luck.picture.lib.entity.LocalMedia;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.picture.utils.AnnexPictureUtil;
import com.zz.yt.lib.annex.popup.PicturePopup;

import java.util.ArrayList;
import java.util.List;


/**
 * 照片获取方式选择
 * <p> 拍照 or 相册</>
 *
 * @author qf
 * @version 2020/6/4
 **/
public final class PicturePopupLocalUtils implements PicturePopup.OnClickListener,
        AnnexPictureUtil.OnClickListener {

    private Activity mActivity;
    private Fragment mFragment;

    /*** 可选择文件数量*/
    private int maxCount = 9;

    /*** 是否单张图片选择，默认是false*/
    private boolean isRadio;

    /*** 是否图片裁剪，默认是false*/
    private boolean isCropping;

    private final OnClickPictureCallback mClickPicture;

    @NonNull
    public static PicturePopupLocalUtils create(OnClickPictureCallback listener) {
        /// 注：有可能是不同控件、不同界面，所以每次需要重新new
        return new PicturePopupLocalUtils(listener);
    }

    private PicturePopupLocalUtils(OnClickPictureCallback listener) {
        this.mClickPicture = listener;
    }

    public PicturePopupLocalUtils setContent(Activity act) {
        this.mActivity = act;
        return this;
    }

    public PicturePopupLocalUtils setContent(Fragment fra) {
        this.mFragment = fra;
        return this;
    }

    public PicturePopupLocalUtils setMaxCount(int maxCount) {
        this.maxCount = maxCount;
        return this;
    }

    /**
     * @param radio: 是否单选
     */
    public PicturePopupLocalUtils setRadio(boolean radio) {
        isRadio = radio;
        return this;
    }

    /**
     * @param cropping: 是否裁剪
     */
    public PicturePopupLocalUtils setCropping(boolean cropping) {
        isCropping = cropping;
        return this;
    }

    /**
     * 拍照
     */
    @Override
    public void requestCamera() {
        setCamera(true);
    }

    /**
     * 相册
     */
    @Override
    public void chooseRequest() {
        setCamera(false);
    }

    public final void popup() {
        PicturePopup.create(Latte.getActivity())
                .setClickListener(this)
                .setPhotograph("拍照")
                .setLocal("相册")
                .popup();
    }

    private void setCamera(boolean camera) {
        if (ObjectUtils.isNotEmpty(mFragment)) {
            AnnexPictureUtil.Bundle()
                    .setContent(mFragment)
                    .setCamera(camera)
                    .setMaxCount(maxCount)
                    .setCropping(isCropping)
                    .setRadio(isRadio)
                    .forResult(this)
                    .init();
            return;
        }
        if (ObjectUtils.isEmpty(mActivity)) {
            mActivity = Latte.getActivity();
        }
        AnnexPictureUtil.Bundle()
                .setContent(mActivity)
                .setCamera(camera)
                .setMaxCount(maxCount)
                .setCropping(isCropping)
                .setRadio(isRadio)
                .forResult(this)
                .init();
    }

    @Override
    public void onClick(List<String> path, ArrayList<LocalMedia> media) {
        if (mClickPicture != null) {
            mClickPicture.onCallback(path);
        }
    }

    @Override
    public void onPreviewDelete(int position) {
        if (mClickPicture != null) {
            mClickPicture.remove(position);
        }
    }
}
