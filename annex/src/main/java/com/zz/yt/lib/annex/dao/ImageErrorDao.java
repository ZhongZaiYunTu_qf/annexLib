package com.zz.yt.lib.annex.dao;

import androidx.annotation.DrawableRes;

import com.whf.android.jar.util.storage.LattePreference;
import com.zz.yt.lib.annex.R;

/**
 * 设置显示异常图片
 *
 * @author qf
 * @version 1.0
 */
public class ImageErrorDao {

    enum error {
        ERROR_HEAD, //默认异常头像地址
        ERROR_IMAGE //默认异常图片地址
    }

    /*** 默认异常头像地址 */
    public static void setErrorHead(@DrawableRes int str) {
        LattePreference.setAppInt(error.ERROR_HEAD.name(), str);
    }

    /*** 默认异常图片地址 */
    public static void setErrorImage(@DrawableRes int str) {
        LattePreference.setAppInt(error.ERROR_IMAGE.name(), str);
    }

    /*** 默认异常头像地址 */
    protected static int getErrorHead() {
        return LattePreference.getAppInt(error.ERROR_HEAD.name(), R.mipmap.iv_default_head_image);
    }

    /*** 默认异常图片地址 */
    protected static int getErrorImage() {
        return LattePreference.getAppInt(error.ERROR_IMAGE.name(), R.mipmap.iv_error_image);
    }

}
