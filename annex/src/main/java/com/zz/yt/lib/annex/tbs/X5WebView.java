package com.zz.yt.lib.annex.tbs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;

/**
 * 自定义webView
 *
 * @author qf
 * @version 1.0
 * @date 2020/11/5
 **/
public class X5WebView extends WebView {


    @SuppressLint("SetJavaScriptEnabled")
    public X5WebView(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
        this.setWebViewClient(new WebViewClient() {
            /**
             * 防止加载网页时调起系统浏览器
             */
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        initWebViewSettings();
        this.getView().setClickable(true);
    }

    /**
     * 初始化webview设置
     */
    private void initWebViewSettings() {

    }

    /**
     * 这里可以为自定义webView绘制背景或文字
     *
     * @param canvas:
     * @param child:
     * @param drawingTime:
     */
    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        boolean ret = super.drawChild(canvas, child, drawingTime);
        canvas.save();
        return ret;
    }

    public X5WebView(Context arg0) {
        super(arg0);
        setBackgroundColor(85621);
    }

}