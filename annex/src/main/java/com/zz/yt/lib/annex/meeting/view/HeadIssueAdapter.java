package com.zz.yt.lib.annex.meeting.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.meeting.entity.IssueEntity;

import java.util.List;


/**
 * 目录适配器
 *
 * @author gogs
 */
class HeadIssueAdapter extends BaseCommonAdapter<IssueEntity> {

    private final String title;

    public HeadIssueAdapter(Context context, String title, List<IssueEntity> mList) {
        super(context, mList);
        this.title = title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CatalogHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.haian_view_issue_catalog, parent, false);
            holder = new CatalogHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (CatalogHolder) convertView.getTag();
        }
        set(holder, mData.get(position), position);
        return convertView;
    }


    @SuppressLint("SetTextI18n")
    private void set(@NonNull CatalogHolder holder, @NonNull IssueEntity issue, int position) {
        //标题栏
        holder.llTitle.setVisibility(position != 0 ? View.GONE : View.VISIBLE);
        holder.mTitle.setText(title);
        //内容
        holder.mSerialNumber.setText("" + (position + 1));
        holder.mIssue.setText(issue.getTheme());
        holder.mPageNumber.setText("" + issue.getNumber());
        holder.mPersonnel.setText(issue.getAttendance());
    }


    static class CatalogHolder extends RecyclerView.ViewHolder {

        public TextView mTitle, mSerialNumber,
                mIssue, mPageNumber, mPersonnel;
        public LinearLayout llTitle, llContent;

        /**
         * 初始化控件
         *
         * @param itemView:元素位置
         */
        public CatalogHolder(View itemView) {
            super(itemView);
            mTitle = itemView.findViewById(R.id.tv_title);
            mSerialNumber = itemView.findViewById(R.id.tv_serial_number);
            mIssue = itemView.findViewById(R.id.tv_issue);
            mPageNumber = itemView.findViewById(R.id.tv_page_number);
            mPersonnel = itemView.findViewById(R.id.tv_personnel);
            llTitle = itemView.findViewById(R.id.ll_title);
            llContent = itemView.findViewById(R.id.ll_content);
        }
    }
}
