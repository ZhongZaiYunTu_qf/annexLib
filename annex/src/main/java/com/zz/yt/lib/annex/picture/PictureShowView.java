package com.zz.yt.lib.annex.picture;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.picture.adapter.PictureShowAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * 图片
 *
 * @author qf
 * @version 2020-01-01
 */
public class PictureShowView extends RelativeLayout {


    private final RecyclerView mRecyclerView;

    private final PictureShowAdapter mPictureAdapter;


    public PictureShowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // 加载布局
        LayoutInflater.from(context).inflate(R.layout.haian_view_picture_show, this, true);
        //Adapter
        mPictureAdapter = new PictureShowAdapter();
        mRecyclerView = findViewById(R.id.recyclerview);
        if (mRecyclerView != null) {
            //每行显示个数，默认3个
            mRecyclerView.setLayoutManager(new GridLayoutManager(context, 3));

            //设置Adapter
            mRecyclerView.setAdapter(mPictureAdapter);
        }
        final TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.PictureShowView);
        if (attributes != null) {

            attributes.recycle();
        }

    }


    public void setData(List<Object> result) {
        if (mPictureAdapter != null) {
            mPictureAdapter.setList(result);
        }
    }

    @Deprecated
    public List<Object> getData() {
        if (mPictureAdapter == null) {
            return new ArrayList<>();
        }
        return mPictureAdapter.getData();
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }


}
