package com.zz.yt.lib.annex.play;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.ObjectUtils;
import com.google.android.exoplayer2.MediaItem;
import com.zz.yt.lib.annex.R;

/**
 * 演示界面(mp4等)
 *
 * @author qf
 * @version 2020-04-08
 */
public class PlayDelegate extends LattePlayDelegate {

    private String url;
    private boolean bool;


    @NonNull
    public static PlayDelegate create(String url) {
        Bundle args = new Bundle();
        args.putString(URL, url);
        PlayDelegate fragment = new PlayDelegate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Bundle args = getArguments();
        if (args != null) {
            url = args.getString(URL);
        }
        if (ObjectUtils.isEmpty(url)) {
            url = "http://vjs.zencdn.net/v/oceans.mp4";
        }
    }

    @Override
    protected Object setLayout() {
        return R.layout.haian_delegate_play;
    }

    @Override
    protected void onBindView(@Nullable Bundle savedInstanceState, @NonNull View rootView) {
        mPlayerView = findViewById(R.id.playerView);
        initPlayer(mPlayerView);
        if (!bool) {
            schedule(MediaItem.fromUri(url), 10);
            bool = true;
        }
    }


}
