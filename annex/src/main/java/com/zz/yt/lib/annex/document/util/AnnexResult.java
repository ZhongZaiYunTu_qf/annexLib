package com.zz.yt.lib.annex.document.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerActivity;
import droidninja.filepicker.FilePickerConst;

/**
 * 自定义文件回调处理。
 *
 * @author qf，gc
 * @version 1.0.2
 */
public class AnnexResult extends ActivityResultContract<Intent, List<String>> {

    @NonNull
    public Intent createIntent(@NonNull Context context, @NonNull Bundle filePickerBuilder) {
        filePickerBuilder.putInt(FilePickerConst.EXTRA_PICKER_TYPE, FilePickerConst.DOC_PICKER);
        Intent intent = new Intent(context, FilePickerActivity.class);
        intent.putExtras(filePickerBuilder);
        return intent;
    }

    @NonNull
    @Override
    public Intent createIntent(@NonNull Context context, Intent intent) {
        return intent;
    }

    @Override
    public List<String> parseResult(int resultCode, Intent intent) {
        if (intent == null || resultCode != Activity.RESULT_OK) {
            return new ArrayList<>();
        }
        final List<String> annex = intent.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);
        final boolean is = annex != null && annex.size() > 0;
        return is ? annex : new ArrayList<String>();
    }
}
