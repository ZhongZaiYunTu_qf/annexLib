package com.zz.yt.lib.annex.meeting.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ColorUtils;
import com.whf.android.jar.change.ChangeListView;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.meeting.entity.IssueEntity;
import com.zz.yt.lib.annex.meeting.utils.OnIssueClickListener;

import java.util.List;

/***
 * 目录
 * @author gogs
 */
public class HeadIssueView extends ChangeListView {

    private final Context context;
    private OnIssueClickListener mIssueClickListener;

    public HeadIssueView(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public HeadIssueView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void setData(String title, List<IssueEntity> issueData) {
        setAdapter(new HeadIssueAdapter(context, title, issueData));
        setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mIssueClickListener != null) {
                    mIssueClickListener.scroll(position);
                }
            }
        });
        setDivider(new ColorDrawable(ColorUtils.getColor(R.color.black)));
        setDividerHeight(1);
    }


    public void setOnClickListener(OnIssueClickListener listener) {
        this.mIssueClickListener = listener;
    }

}
