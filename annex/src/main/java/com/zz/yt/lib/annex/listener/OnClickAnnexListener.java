package com.zz.yt.lib.annex.listener;

import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.document.constants.AnnexKeys;

/**
 * 附件
 *
 * @author qf
 * @version 2020/7/31
 **/
public interface OnClickAnnexListener {


    /**
     * 点击预览
     *
     * @param type:到签字,图片,到tbs
     * @param item:参数
     */
    void onClickAnnex(@AnnexKeys.TYPE int type, AnnexBean item);
}
