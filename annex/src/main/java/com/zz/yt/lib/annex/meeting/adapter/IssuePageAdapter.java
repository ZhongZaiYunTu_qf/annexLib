package com.zz.yt.lib.annex.meeting.adapter;

import android.text.Html;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.blankj.utilcode.util.ObjectUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.zz.yt.lib.annex.R;
import com.zz.yt.lib.annex.document.DocumentView;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.listener.OnClickAnnexLoading;
import com.zz.yt.lib.annex.meeting.entity.IssueEntity;
import com.zz.yt.lib.annex.utils.HeadUtils;

import java.util.ArrayList;
import java.util.List;

/***
 * 议案阅读器
 * 上下滑动型
 * @author gogs
 */
public class IssuePageAdapter extends BaseQuickAdapter<IssueEntity, BaseViewHolder> {

    private final OnClickAnnexLoading mClickAnnexLoading;

    public IssuePageAdapter(List<IssueEntity> mList, OnClickAnnexLoading listener) {
        super(R.layout.haian_item_issue, mList);
        this.mClickAnnexLoading = listener;
    }

    /***
     * 显示附件
     * @param holder:
     */
    private void initDocument(BaseViewHolder holder, List<String> data) {
        final int size = data == null ? 0 : data.size();
        final ArrayList<AnnexBean> annex = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            annex.add(new AnnexBean(data.get(i)));
        }
        final DocumentView document = holder.getView(R.id.id_document_view);
        if (ObjectUtils.isNotEmpty(document)) {
            document.setEdit(false);
            document.setData(annex);
            document.setOnClickAnnexLoading(mClickAnnexLoading);
        }
    }

    @Override
    protected void convert(@NonNull BaseViewHolder holder, @NonNull IssueEntity issueEntity) {
        final boolean headGone = issueEntity.getType() == 0;
        holder.setGone(R.id.ll_head, !headGone);
        holder.setGone(R.id.img_image, headGone);
        if (headGone) {
            final String issue = issueEntity.getIssue();
            holder.setText(R.id.tv_head, issueEntity.getHead())
                    .setText(R.id.tv_head_one, issueEntity.getHeadOne())
                    .setText(R.id.tv_theme, issueEntity.getTheme())
                    .setText(R.id.tv_issue, Html.fromHtml(issue))
                    .setText(R.id.reportDept, "呈报部门:  " + issueEntity.getReportDept())
                    .setText(R.id.reportTime, "呈报时间:  " + issueEntity.getReportTime())
                    .setText(R.id.reportUser, "汇  报  人:  " + issueEntity.getReportUser())
                    .setText(R.id.reportAttend, "列  席:  " + issueEntity.getAttendance())
                    .setText(R.id.reportTel, "联系电话:  " + issueEntity.getTel());
            final List<String> data = issueEntity.getFiles();
            initDocument(holder, data);
        } else {
            final ImageView imageView = holder.getView(R.id.img_image);
            HeadUtils.image(imageView, issueEntity.getUrl());
        }
    }
}
