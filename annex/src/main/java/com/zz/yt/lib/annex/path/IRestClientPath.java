package com.zz.yt.lib.annex.path;

import android.text.TextUtils;

import com.whf.android.jar.net.gson.HttpGsonUtils;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.picture.bean.FilePathBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 上传文件类。
 *
 * @author qf
 * @version 1.0.3
 */
public abstract class IRestClientPath {

    /**
     * @param select:获取数据
     * @param callback:回调
     */
    public abstract void setSelect(List<String> select, OnClickAnnexCallback callback);

    /**
     * @param data:json解析
     * @param callback:回调
     */

    protected void onGson(String data, OnClickAnnexCallback callback) {
        List<AnnexBean> annexBeanList = new ArrayList<>();
        if (!TextUtils.isEmpty(data) && data.length() > 1) {
            if ("[".equals(data.substring(0, 1))) {
                final List<FilePathBean> filePathArray = HttpGsonUtils.fromJsonList(data, FilePathBean.class);
                if (filePathArray != null) {
                    for (FilePathBean filePathBean : filePathArray) {
                        annexBeanList.add(new AnnexBean(filePathBean.getId(),
                                filePathBean.getFileName(),
                                filePathBean.getPath()));
                    }
                }

            } else {
                final FilePathBean filePathBean = HttpGsonUtils.fromJson(data, FilePathBean.class);
                if (filePathBean != null) {
                    annexBeanList.add(new AnnexBean(filePathBean.getId(),
                            filePathBean.getFileName(),
                            filePathBean.getPath()));
                }
            }
        }
        if (callback != null) {
            callback.filePath(annexBeanList);
        }
    }

    /**
     * 回调
     */
    public interface OnClickAnnexCallback {

        /**
         * @param filePath：上传结果回调
         */
        void filePath(List<AnnexBean> filePath);
    }
}
