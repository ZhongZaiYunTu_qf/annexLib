package com.zz.yt.test;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;


import androidx.core.content.ContextCompat;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.annex.QbSdkUtils;
import com.zz.yt.lib.annex.loader.ImageLoaderUtils;
import com.zz.yt.lib.annex.popup.PicturePopup;
import com.zz.yt.lib.annex.tbs.X5WebActivity;
import com.zz.yt.test.spread.AnnexActivity;
import com.zz.yt.test.spread.ImageLoaderActivity;
import com.zz.yt.test.spread.OpinionActivity;
import com.zz.yt.test.spread.PdfJsActivity;
import com.zz.yt.test.spread.PictureActivity;
import com.zz.yt.test.spread.PictureWatermarkActivity;
import com.zz.yt.test.spread.PlayActivity;
import com.zz.yt.test.spread.SpreadActivity;
import com.zz.yt.test.spread.VideoActivity;


/**
 * @author qf
 */
public class MainActivity extends Activity {

    View main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Latte.getConfigurator().withActivity(this);
        main = findViewById(R.id.main);
    }

    public void onClick(View view) {
        //图片选择
        ActivityUtils.startActivity(PictureActivity.class);
    }

    public void onClick0(View view) {
        //图片选择-添加水印
        ActivityUtils.startActivity(PictureWatermarkActivity.class);
    }

    public void onClick1(View view) {
        ActivityUtils.startActivity(AnnexActivity.class);
    }

    public void onClick2(View view) {
        //文件、会议展示
        ActivityUtils.startActivity(SpreadActivity.class);
    }

    public void onClick3(View view) {
        ActivityUtils.startActivity(OpinionActivity.class);
    }

    public void onClick4(View view) {
        PicturePopup.create(this).popup();
    }

    public void onClick5(View view) {
        QbSdkUtils.openFileReader(this, "https://img-blog.csdnimg.cn/5eb39ba135e644c6830e56a47ece3daf.png",
                null, null);
    }

    public void onClick51(View view) {
        ActivityUtils.startActivity(PdfJsActivity.class);
    }

    public void onClick6(View view) {
        ImageLoaderUtils.create(null, "https://img-blog.csdnimg.cn/5eb39ba135e644c6830e56a47ece3daf.png");
    }

    public void onClick7(View view) {
        ActivityUtils.startActivity(ImageLoaderActivity.class);
    }


    public void onClick8(View view) {
        ActivityUtils.startActivity(VideoActivity.class);
    }

    public void onClick9(View view) {
        X5WebActivity.start(this, "http://vjs.zencdn.net/v/oceans.mp4");
    }

    public void onClick10(View view) {
        ActivityUtils.startActivity(PlayActivity.class);
    }

    /**
     * @param view:android 唤醒别人APP、传递token
     */
    public void onClick11(View view) {
        String url = "https://gxapp.shzgx.com/download/apk/ce4bb67802658565cde3ed923fcc34c1";
        // 假设token是你想要传递的数据
        String token = "your_token_here";
        String pkg = "com.hngx.sc";

        // 创建一个新的Intent
        Intent intent = new Intent();

        // 设置目标应用的包名和Activity名（确保这些是正确的）
        intent.setComponent(new ComponentName(pkg, pkg + ".LaunchActivity"));

        // 添加你想要传递的数据到Intent中
        intent.putExtra("token", token);

        try {
            // 启动Intent
            ContextCompat.startActivity(this, intent, null);
        } catch (ActivityNotFoundException e) {
            ToastUtils.showLong("找不到目标Activity的情况");
//            // 处理找不到目标Activity的情况, 可以提示用户安装目标应用或做其他处理
//            Intent intentUri = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            ContextCompat.startActivity(this, intentUri, null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        QbSdkUtils.closeFileReader(this);
    }
}
