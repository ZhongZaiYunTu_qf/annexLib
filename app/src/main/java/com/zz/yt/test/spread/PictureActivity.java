package com.zz.yt.test.spread;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.blankj.utilcode.util.GsonUtils;
import com.whf.android.jar.base.latte.BaseActivity;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.picture.PictureView;
import com.zz.yt.lib.annex.popup.PicturePopup;
import com.zz.yt.test.R;

import java.util.List;


/**
 * 图片
 *
 * @author qf
 * @version 2020-09-25
 */
public class PictureActivity extends BaseActivity {

    View main;

    @Override
    protected Object setLayout() {
        return R.layout.activity_picture;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState) {
        main = findViewById(R.id.main);
        final PictureView pictureView0 = findViewById(R.id.pictureView_a);
        pictureView0.isCamera(true).setContent(this);

        final PictureView pictureView = findViewById(R.id.pictureView);
        pictureView.isCamera(true).setContent(this).setCropping(true);
        pictureView.setClickPictureCallback(new OnClickPictureCallback() {
            @Override
            public void onCallback(List<String> selectList) {
                LatteLogger.json(GsonUtils.toJson(selectList));
            }

            @Override
            public void remove(int position) {

            }
        });
    }

    public void onClick(View view) {
        PicturePopup.create(this).popup();
    }
}
