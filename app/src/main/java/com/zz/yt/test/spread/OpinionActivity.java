package com.zz.yt.test.spread;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.zz.yt.lib.annex.opinion.OpinionFlowView;
import com.zz.yt.lib.annex.opinion.bean.OpinionFlowBean;
import com.zz.yt.test.R;

import java.util.ArrayList;

/**
 * 意见区
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/10/29
 **/
public class OpinionActivity extends Activity {

    private OpinionFlowView mOpinionFlowView = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opinion);
        mOpinionFlowView = findViewById(R.id.id_opinion_flow_view);
        setOpinionFlowView();
    }

    private void setOpinionFlowView() {
        final ArrayList<OpinionFlowBean> annex = new ArrayList<>();
        annex.add(new OpinionFlowBean(R.drawable.tree_select_on, "陈敏", "2020-09-25", "同意",
                "意见的本意是人们对事物所产生的看法或想法。意见是上级领导机关对下级机关部署工作，指导下级机关工作活动的原则、步骤和方法的一种文体。意见的指导性很强，有时是针对当时带有普遍性的问题发布的，有时是针对局部性的问题而发布的，意见往往在特定的时间内发生效力。"));
        annex.add(new OpinionFlowBean(R.drawable.tree_select_on, "陈敏", "2020-09-25", "同意", ""));
        annex.add(new OpinionFlowBean(R.drawable.tree_select_on, "陈敏", "2020-09-25", "同意", ""));
        mOpinionFlowView.setData(annex);
    }

    public void onClick(View view) {
        mOpinionFlowView.setAvatar(false);
        setOpinionFlowView();
    }

    public void onClick1(View view) {
        mOpinionFlowView.setAvatar(true);
        setOpinionFlowView();
    }
}
