package com.zz.yt.test.app;

import android.app.Application;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.dao.LocateDao;
import com.whf.android.jar.net.HttpCode;
import com.zz.yt.lib.annex.QbSdkUtils;
import com.zz.yt.test.BuildConfig;
import com.zz.yt.test.R;

/**
 * @author oa
 * @version 2020-05-29
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        //全局初始化
        Latte.init(this)
                .withLoaderDelayed(1000)
                .withAdaptation(360f)
                .withFtpHost("http://www.baidu.com")
                .withWebHost("http://www.baidu.com")
                .withApiHost("http://www.baidu.com")
                .withLoggable(BuildConfig.DEBUG)
                .withVersionCode(BuildConfig.VERSION_CODE)
                .withVersionName(BuildConfig.VERSION_NAME)
                .withApplicationId(BuildConfig.APPLICATION_ID)
                .withSignExpiration(HttpCode.CODE_403)
                .withJavascriptInterface("ja.ja.ja")
                .withAppIcon(R.drawable.app_core_icon)
                .withInterceptor()
                .configure();

        QbSdkUtils.init();
        LocateDao.setAddress("内蒙古自治区呼伦贝尔市莫力达瓦达斡尔族自治旗杜拉尔鄂温克民族乡尼西空海拉松村");
    }
}
