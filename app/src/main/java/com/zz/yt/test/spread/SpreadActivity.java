package com.zz.yt.test.spread;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.zz.yt.lib.annex.document.DocumentGridView;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.meeting.IssuePageView;
import com.zz.yt.test.R;

import java.util.ArrayList;

/**
 * @author qf
 * @date 2020-09-25
 */
public class SpreadActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spread);
        DocumentGridView gridView = findViewById(R.id.id_document_view);

        ArrayList<AnnexBean> annex = new ArrayList<>();
        annex.add(new AnnexBean("/assassin/文件.java"));
        annex.add(new AnnexBean("/assassin/文件.pdf"));
        annex.add(new AnnexBean("/assassin/文件.jpg"));
        gridView.setData(annex);

        IssuePageView pageView = findViewById(R.id.id_issue_page_view);
    }


}
