package com.zz.yt.test.spread;


import android.os.Bundle;

import androidx.annotation.Nullable;

import com.whf.android.jar.base.delegate.BaseDelegate;
import com.whf.android.jar.base.latte.BaseProxyActivity;
import com.zz.yt.lib.annex.play.PlayDelegate;

/**
 * play activity
 *
 * @author qf
 * @version 1.0.3
 */
public class PlayActivity extends BaseProxyActivity {

    @Override
    public BaseDelegate setRootDelegate() {
        return new PlayDelegate();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
}
