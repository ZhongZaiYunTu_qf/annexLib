package com.zz.yt.test.spread;


import android.text.TextUtils;

import com.whf.android.jar.base.delegate.BaseDelegate;
import com.whf.android.jar.base.latte.BaseProxyActivity;
import com.whf.android.jar.web.WebDelegateImpl;

public class PdfJsActivity extends BaseProxyActivity {

    @Override
    public BaseDelegate setRootDelegate() {
        String url = getIntent().getStringExtra("url");
        if (TextUtils.isEmpty(url)) {
            url = "file:///android_asset/wj/文件模板.pdf";
        }
        WebDelegateImpl delegate = WebDelegateImpl.create("file:///android_asset/pdf/show_pdf.html?"
                + url);
        delegate.setPageLoadListener(this);
        return delegate;
    }
}
