package com.zz.yt.test.spread;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.tencent.smtt.sdk.TbsVideo;
import com.tencent.smtt.sdk.WebChromeClient;
import com.whf.android.jar.app.Latte;
import com.zz.yt.lib.annex.QbSdkUtils;
import com.zz.yt.lib.annex.tbs.X5WebView;
import com.zz.yt.test.R;

/**
 * 视频播放器
 *
 * @author qf
 * @author 中再云图技术有限公司
 * @version 1.0
 * @date 2020/11/5
 **/
public class VideoActivity extends Activity {

    X5WebView x5webView;
    WebView webView;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        x5webView = findViewById(R.id.x5_web_view);
        webView = findViewById(R.id.web_view);
        EditText editText = findViewById(R.id.id_et_text);
        editText.setText("http://vjs.zencdn.net/v/oceans.mp4");
        startPlay("http://vjs.zencdn.net/v/oceans.mp4");
    }

    /**
     * 使用自定义webview播放视频
     *
     * @param videoUrl 视频地址
     */
    private void startPlay(String videoUrl) {
        webView.loadUrl(videoUrl);
        x5webView.loadUrl(videoUrl);
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        x5webView.getView().setOverScrollMode(View.OVER_SCROLL_ALWAYS);
        x5webView.setWebChromeClient(new WebChromeClient());
    }

    public void onClick(View view) {
        EditText editText = findViewById(R.id.id_et_text);
        startPlay(editText.getText().toString().trim());
    }

    public void onClick1(View view) {
        EditText editText = findViewById(R.id.id_et_text);
        boolean is = QbSdkUtils.inFlue(editText.getText().toString().trim());
    }
}
