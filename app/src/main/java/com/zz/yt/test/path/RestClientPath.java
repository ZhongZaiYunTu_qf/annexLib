package com.zz.yt.test.path;

import com.whf.android.jar.app.Latte;
import com.whf.android.jar.net.utils.RestClientUtils;
import com.zz.yt.lib.annex.path.IRestClientPath;

import java.util.List;

/**
 * 默认提供上传解析的工具。
 *
 * @author qf
 * @version 1.0.3
 */
public class RestClientPath extends IRestClientPath {


    public void setSelect(List<String> select, final OnClickAnnexCallback callback) {
        String apiFileUpload = Latte.getField() + "/file/upload";
        RestClientUtils.create().inPath(apiFileUpload, select, data -> onGson(data, callback));
    }

    @Override
    protected void onGson(String data, OnClickAnnexCallback callback) {
        super.onGson(data, callback);
    }


}
