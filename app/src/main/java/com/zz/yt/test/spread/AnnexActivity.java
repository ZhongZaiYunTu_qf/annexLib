package com.zz.yt.test.spread;

import android.os.Bundle;

import androidx.activity.result.ActivityResult;
import androidx.annotation.Nullable;

import com.blankj.utilcode.util.GsonUtils;
import com.whf.android.jar.base.latte.BaseActivity;
import com.whf.android.jar.util.log.LatteLogger;
import com.zz.yt.lib.annex.document.DocumentView;
import com.zz.yt.lib.annex.document.bean.AnnexBean;
import com.zz.yt.lib.annex.listener.OnClickAnnexLoading;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.constant.PictureConstant;
import com.zz.yt.test.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qf
 * @version 2020-05-16
 */
public class AnnexActivity extends BaseActivity implements OnClickAnnexLoading {

    private DocumentView mDocumentView;

    @Override
    protected Object setLayout() {
        return R.layout.activity_annex;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState) {
        mDocumentView = findViewById(R.id.id_document_view);
        mDocumentView.setRootView(PictureConstant.ALL);
        mDocumentView.setOnClickAnnexLoading(this);
        mDocumentView.setContext(this);
        mDocumentView.setMaxCount(9);
        mDocumentView.setPreview("提示不可以查看文件的理由");
        mDocumentView.setActivityResultLauncher(startLauncher);
        mDocumentView.setClickPictureCallback(new OnClickPictureCallback() {
            @Override
            public void onCallback(List<String> selectList) {
                for (String str : selectList) {
                    mDocumentView.addData(new AnnexBean(str));
                }
            }

            @Override
            public void remove(int position) {
                LatteLogger.json(GsonUtils.toJson(mDocumentView.getData()));
            }
        });
        final ArrayList<AnnexBean> annex = new ArrayList<>();
        annex.add(new AnnexBean("file:///android_asset/wj/文件模板.pdf"));
        annex.add(new AnnexBean("file:///android_asset/wj/文件模板.docx"));
        mDocumentView.setData(annex);
    }

    @Override
    public void onActivityResult(ActivityResult result) {
        if (mDocumentView != null) {
            mDocumentView.onActivityResult(result);
        }
    }
}
