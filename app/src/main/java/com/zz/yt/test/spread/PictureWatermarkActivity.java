package com.zz.yt.test.spread;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.whf.android.jar.base.latte.BaseActivity;
import com.zz.yt.lib.annex.listener.OnClickPictureCallback;
import com.zz.yt.lib.annex.popup.utils.PicturePopupLocalUtils;
import com.zz.yt.lib.annex.utils.HeadUtils;
import com.zz.yt.lib.annex.utils.ImageWaterMarkUtil;
import com.zz.yt.test.R;

import java.util.List;


/**
 * 图片选择-添加水印
 *
 * @author qf
 * @version 2020-09-25
 */
public class PictureWatermarkActivity extends BaseActivity implements OnClickPictureCallback {

    ImageView imageView;

    @Override
    protected Object setLayout() {
        return R.layout.activity_picture_watermark;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState) {
        imageView = findViewById(R.id.id_image);
    }

    public void onClick(View view) {
        PicturePopupLocalUtils.create(this)
                .setContent(this)
                .setCropping(false)
                .setRadio(true)
                .popup();
    }

    @Override
    public void onCallback(List<String> selectList) {
        if (selectList != null && selectList.size() > 0) {
            HeadUtils.image(imageView, ImageWaterMarkUtil.watermarkBitmap(selectList.get(0)));
        }
    }

    @Override
    public void remove(int position) {

    }
}
