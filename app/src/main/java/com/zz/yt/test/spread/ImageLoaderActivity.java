package com.zz.yt.test.spread;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.whf.android.jar.base.latte.BaseActivity;
import com.zz.yt.lib.annex.loader.ImageLoaderArrayUtils;
import com.zz.yt.lib.annex.picture.PictureShowView;
import com.zz.yt.test.R;

import java.util.ArrayList;
import java.util.List;

public class ImageLoaderActivity extends BaseActivity {
    final List<Object> objectList = new ArrayList<>();

    @Override
    protected Object setLayout() {
        return R.layout.activity_image_loader;
    }

    @Override
    public void onBindView(@Nullable Bundle savedInstanceState) {
        objectList.add("https://img-blog.csdnimg.cn/6d15082ac7234ec7a16065e74f689590.jpeg");
        objectList.add("https://img-blog.csdnimg.cn/3d809148c83f4720b5e2a6567f816d89.jpeg");
        objectList.add("https://img-blog.csdnimg.cn/5a26bb2e6eb74aa0ac42d44bef0f61c2.jpeg");
        objectList.add("https://img-blog.csdnimg.cn/fcc22710385e4edabccf2451d5f64a99.jpeg");
        objectList.add("");
        objectList.add("https://img-blog.csdnimg.cn/e469a7c53f274934abc357af019c90d4.jpeg");
        objectList.add("https://img-blog.csdnimg.cn/5eb39ba135e644c6830e56a47ece3daf.png");
        @SuppressLint("WrongViewCast")
        PictureShowView showView = findViewById(R.id.id_image_picture);
        showView.setData(objectList);
    }


    public void onClick1(View view) {
        ImageLoaderArrayUtils.create(null, objectList)
                .setCurrentPosition(1)
                .show();
    }

}
