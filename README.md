# annex 版本

[![](https://jitpack.io/v/com.gitee.manypeaks/annexLib.svg)](https://jitpack.io/#com.gitee.manypeaks/annexLib)


#### 介绍
附件


#### 安装教程

1.  使用九宫格布局添加图片

	1.1.布局文件添加
	
        <com.zz.yt.lib.annex.picture.PictureView
            android:id="@+id/pictureView"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:addition="gallery" />

		
	1.2 设置回调和view
	
        pictureView = findViewById(R.id.pictureView);
        pictureView.setRootView(main);
        pictureView.setClickPictureCallback(this);
			
	1.3 属性
		
        <!--总共个数，默认9个-->
        <attr name="max_display_count" format="integer" />
        <!--每行显示个数，默认3个-->
        <attr name="each_line_count" format="integer" />
        <!--是否显示添加，默认是true-->
        <attr name="is_added" format="boolean" />
        <!--是否能点击预览，默认是true-->
        <attr name="is_preview" format="boolean" />
        <!--添加图片方式-->
        <attr name="addition" format="enum">
            <!--拍照-->
            <enum name="camera" value="1" />
            <!--相册-->
            <enum name="gallery" value="2" />
            <!--拍照和相册-->
            <enum name="all" value="3" />
        </attr>
        
	
2.  使用头像更换工具
	
        @Override
        protected void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            ....
            HeadPortraitUtils.create(mView, mImageView, this);
            ....
                    
        }
	
	
3.  使用tbs预览服务
				
	3.1 初始化
	
            public class App extends Application {

                @Override
                public void onCreate() {
                    super.onCreate();
            
                    //全局初始化
                    Latte.init(this).configure();
            		
            		......
            
                    QbSdkUtils.init();
            		......
                } 
            }
    				
	3.2 开始使用	
	
	    public void onClick5(View view) {
                QbSdkUtils.openFileReader(this,"/storage/emulated/0/down_loads/派车单审核统计表.xls",null,null);
            }
	
	
	3.3 结束使用
	
	    @Override
            protected void onDestroy() {
                super.onDestroy();
                QbSdkUtils.closeFileReader(this);
            }
	 
	 

4.  使用图片预览服务

        public void onClick6(View view) {
            ImageLoaderUtils.create(null, "");
    		//or
            ImageLoaderUtils.create(mImageView, "");
        }

        public void onClick7(View view) {
            List<Object> objectList = new ArrayList<>();
            objectList.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598592802972&di=3987ba74ffdddb1274f16b9bc4168b5e&imgtype=0&src=http%3A%2F%2Fa3.att.hudong.com%2F14%2F75%2F01300000164186121366756803686.jpg");
            objectList.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598592802971&di=a704829619c87cbbe6934dd21b5e9186&imgtype=0&src=http%3A%2F%2Fa0.att.hudong.com%2F56%2F12%2F01300000164151121576126282411.jpg");
            objectList.add("https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1598593430218&di=954462a126c0d347f6e3d865fdddb38f&imgtype=0&src=http%3A%2F%2Fd.paper.i4.cn%2Fmax%2F2016%2F12%2F01%2F10%2F1480558471363_838559.jpg");
            objectList.add("");
            ImageLoaderArrayUtils.create(null, objectList)
                    .setCurrentPosition(1)
                    .show();
        }
    

#### 使用说明
